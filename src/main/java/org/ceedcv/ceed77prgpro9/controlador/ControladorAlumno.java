package org.ceedcv.ceed77prgpro9.controlador;

import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.ceedcv.ceed77prgpro9.modelo.Alumno;
import org.ceedcv.ceed77prgpro9.modelo.IModelo;
import org.ceedcv.ceed77prgpro9.vista.VistaGraficaAlumno;
import org.ceedcv.ceed77prgpro9.vista.Funciones;
import org.ceedcv.ceed77prgpro9.vista.Verificador;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class ControladorAlumno implements ActionListener, MouseListener {
    private IModelo modelo;
    private VistaGraficaAlumno vista;
    private ArrayList alumnos;
    private String chivato;
    private Alumno actual;
    private int indice;
    
    public ControladorAlumno(IModelo m, VistaGraficaAlumno v) {
        //Asignamos el modelo y la vista
        modelo = m;
        vista = v;
        
        //Añadimos los listeners
        vista.getSalir().addActionListener(this);
        vista.getGuardar().addActionListener(this);
        vista.getCancelar().addActionListener(this);
        vista.getCreate().addActionListener(this);
        vista.getUpdate().addActionListener(this);
        vista.getRead().addActionListener(this);
        vista.getDelete().addActionListener(this);
        vista.getSearch().addActionListener(this);
        vista.getPrimero().addActionListener(this);
        vista.getAnterior().addActionListener(this);
        vista.getSiguiente().addActionListener(this);
        vista.getUltimo().addActionListener(this);
        
        vista.getSalir().addMouseListener(this);
        vista.getGuardar().addMouseListener(this);
        vista.getCancelar().addMouseListener(this);
        vista.getCreate().addMouseListener(this);
        vista.getUpdate().addMouseListener(this);
        vista.getRead().addMouseListener(this);
        vista.getDelete().addMouseListener(this);
        vista.getSearch().addMouseListener(this);
        vista.getPrimero().addMouseListener(this);
        vista.getAnterior().addMouseListener(this);
        vista.getSiguiente().addMouseListener(this);
        vista.getUltimo().addMouseListener(this);       
    }
    
    //Implementamos los métodos de las interfaces de eventos
    @Override
    public void actionPerformed(ActionEvent e) { 
        Object objeto = e.getSource();
        
        if (objeto == vista.getSalir()) {
            salir();
        } else if (objeto == vista.getCreate()) {
            create();
        } else if (objeto == vista.getRead()) {
            read();
        } else if (objeto == vista.getUpdate()) {
            update();
        } else if (objeto == vista.getDelete()) {
            delete();
        } else if (objeto == vista.getSearch()) {
            search();
        } else if (objeto == vista.getPrimero()) {
            primero();
        } else if (objeto == vista.getAnterior()) {
            anterior();
        } else if (objeto == vista.getSiguiente()) {
            siguiente();
        } else if (objeto == vista.getUltimo()) {
            ultimo();
        } else if (objeto == vista.getGuardar()) {
            guardar(chivato);
        } else if (objeto == vista.getCancelar()) {
            cancelar();
        }
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
        Object objeto = e.getSource();
        
        if (objeto == vista.getSalir()) {
            vista.getInfo().setText("Vuelve al menú principal");
        } else if (objeto == vista.getCreate()) {
            vista.getInfo().setText("Permite dar de alta un nuevo alumno");        
        } else if (objeto == vista.getRead()) {
            vista.getInfo().setText("Permite navegar la lista de alumnos");
        } else if (objeto == vista.getUpdate()) {
            vista.getInfo().setText("Permite actualizar los datos del alumno");
        } else if (objeto == vista.getDelete()) {
            vista.getInfo().setText("Borra el alumno en pantalla");
        } else if (objeto == vista.getSearch()) {
            vista.getInfo().setText("Busca a un alumno a partir de su ID. Pulsa 'Guardar' para buscar. 'Cancelar' para salir");
        } else if (objeto == vista.getPrimero()) {
            vista.getInfo().setText("Muestra al primer alumno de la lista");
        } else if (objeto == vista.getAnterior()) {
            vista.getInfo().setText("Muestra al alumno anterior");
        } else if (objeto == vista.getSiguiente()) {
            vista.getInfo().setText("Muestra al alumno siguiente");
        } else if (objeto == vista.getUltimo()) {
            vista.getInfo().setText("Muestra el último alumno de la lista");
        } else if (objeto == vista.getGuardar()) {
            vista.getInfo().setText("Guarda la información");
        } else if (objeto == vista.getCancelar()) {
            vista.getInfo().setText("Cancela la operación en curso");
        }
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
        Object objeto = e.getSource();
        
        if (objeto == vista.getSalir() || objeto == vista.getCreate()
            || objeto == vista.getRead() || objeto == vista.getUpdate()
            || objeto == vista.getDelete() || objeto == vista.getSearch()
            || objeto == vista.getPrimero() || objeto == vista.getAnterior()
            || objeto == vista.getSiguiente() || objeto == vista.getUltimo()
            || objeto == vista.getGuardar() || objeto == vista.getCancelar()) {
                vista.getInfo().setText("MENÚ ALUMNO");
        }
    }
    
    public void mousePressed(MouseEvent e) {
        //No hacer nada
    }
    public void mouseReleased(MouseEvent e) {
        //No hacer nada
    }
    public void mouseClicked(MouseEvent e) {
        //No hacer nada
    }
    
    //Configuramos el comportamiento del menú CRUD
    private void create() {
        chivato = "create";
        vista.getTid().setEnabled(false);
        vista.getTnombre().setEnabled(true);
        vista.getTdni().setEnabled(true);
        vista.getTedad().setEnabled(true);
        vista.getTemail().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getRead().setEnabled(false);
        vista.getSearch().setEnabled(false);
        validaciones();
    }
    
    private void read() {
        alumnos = modelo.readAlumno();
        vista.getPrimero().setEnabled(true);
        vista.getAnterior().setEnabled(true);
        vista.getSiguiente().setEnabled(true);
        vista.getUltimo().setEnabled(true);
        vista.getCreate().setEnabled(false);
        vista.getRead().setEnabled(false);
        vista.getSearch().setEnabled(false);
        vista.getUpdate().setEnabled(true);
        vista.getDelete().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        primero();
    }
    
    private void update() {
        chivato = "update";
        vista.getTnombre().setEnabled(true);
        vista.getTdni().setEnabled(true);
        vista.getTedad().setEnabled(true);
        vista.getTemail().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getPrimero().setEnabled(false);
        vista.getAnterior().setEnabled(false);
        vista.getSiguiente().setEnabled(false);
        vista.getUltimo().setEnabled(false);
        validaciones();
    }
    
    private void delete() {
        Alumno alumno = alta();
        modelo.delete(alumno);
        cancelar();
    }
    
    private void search() {
        chivato = "search";
        vista.getTid().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getCreate().setEnabled(false);
        vista.getRead().setEnabled(false);
    }
    
    //Configuramos el comportamiento del menú de navegabilidad
    private void primero() {
        if (alumnos != null) {
            indice = 0;
            actual = ((Alumno)alumnos.get(indice));   
            vista.getTid().setText(actual.getId());
            vista.getTnombre().setText(actual.getNombre());
            vista.getTdni().setText(actual.getDni());
            vista.getTedad().setText(Integer.toString(actual.getEdad()));
            vista.getTemail().setText(actual.getEmail());
        } else {
            actual = null;
            indice = -1;
        }
    }
        
    private void anterior()  {
        if (indice != 0)     {
            indice -= 1;
            actual = ((Alumno)alumnos.get(indice));
            vista.getTid().setText(actual.getId());
            vista.getTnombre().setText(actual.getNombre());
            vista.getTdni().setText(actual.getDni());
            vista.getTedad().setText(Integer.toString(actual.getEdad()));
            vista.getTemail().setText(actual.getEmail());
        }
    }

    private void siguiente() {
        if (indice != alumnos.size() - 1) {
            indice += 1;
            actual = ((Alumno)alumnos.get(indice));
            vista.getTid().setText(actual.getId());
            vista.getTnombre().setText(actual.getNombre());
            vista.getTdni().setText(actual.getDni());
            vista.getTedad().setText(Integer.toString(actual.getEdad()));
            vista.getTemail().setText(actual.getEmail());
        }
    }
  
    private void ultimo() {
        indice = (alumnos.size() - 1);
        actual = ((Alumno)alumnos.get(indice));
        vista.getTid().setText(actual.getId());
        vista.getTnombre().setText(actual.getNombre());
        vista.getTdni().setText(actual.getDni());
        vista.getTedad().setText(Integer.toString(actual.getEdad()));
        vista.getTemail().setText(actual.getEmail());
    }
    
    //Configuramos el comportamiento de los botones guardar, cancelar y salir
    private void guardar(String chivato) {
        Alumno alumno = new Alumno();
        Funciones f = new Funciones();
        switch (chivato) {
            case "create":
                try {
                    alumno = alta();
                    modelo.create(alumno);
                    cancelar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "update":
                try {
                    alumno = alta();
                    modelo.update(alumno);
                    cancelar();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "search":
                try {
                    alumno.setId(vista.getTid().getText());
                    Alumno a = modelo.search(alumno);
                    vista.getTnombre().setText(a.getNombre());
                    vista.getTdni().setText(a.getDni());
                    vista.getTedad().setText(Integer.toString(a.getEdad()));
                    vista.getTemail().setText(a.getEmail());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }
    
    private void cancelar() {
        //Deshabilitamos campos de texto 
        vista.getTid().setEnabled(false);
        vista.getTnombre().setEnabled(false);
        vista.getTdni().setEnabled(false);
        vista.getTedad().setEnabled(false);
        vista.getTemail().setEnabled(false);
        //Limpiamos campos de texto
        vista.getTid().setText("");
        vista.getTnombre().setText("");
        vista.getTdni().setText("");
        vista.getTedad().setText("");
        vista.getTemail().setText("");
        //Configuramos guardar, cancelar
        vista.getGuardar().setEnabled(false);
        vista.getCancelar().setEnabled(false);
        //Configuramos CRUD
        vista.getCreate().setEnabled(true);
        vista.getRead().setEnabled(true);
        vista.getUpdate().setEnabled(false);
        vista.getDelete().setEnabled(false);
        vista.getSearch().setEnabled(true);
        //Configuramos navegabilidad
        vista.getPrimero().setEnabled(false);
        vista.getAnterior().setEnabled(false);
        vista.getSiguiente().setEnabled(false);
        vista.getUltimo().setEnabled(false);
    }
    
    private void salir() {
        try {
            vista.setSelected(false);
            vista.dispose();
        } catch (PropertyVetoException ex) {
            Logger.getLogger(ControladorAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Otras funciones
    private Alumno alta() {
        String id = vista.getTid().getText();
        String nombre = vista.getTnombre().getText();
        String dni = vista.getTdni().getText();
        int edad = Integer.parseInt(vista.getTedad().getText());
        String email = vista.getTemail().getText();
        Alumno alumno = new Alumno(id, nombre, dni, edad, email);
        return alumno;
    }
    
    private void validaciones() {
        vista.getTnombre().setInputVerifier(new Verificador("null"));
        vista.getTdni().setInputVerifier(new Verificador("tdni"));
        vista.getTedad().setInputVerifier(new Verificador("tedad"));
        vista.getTemail().setInputVerifier(new Verificador("temail"));
    }
}