package org.ceedcv.ceed77prgpro9.controlador;

import java.io.IOException;
import org.ceedcv.ceed77prgpro9.modelo.IModelo;
import org.ceedcv.ceed77prgpro9.vista.VistaGrafica;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Main {
    public static void main (String[] args) throws IOException {
        IModelo modelo = null;
        VistaGrafica vista = null;
        Controlador controlador = new Controlador (modelo, vista);
    }
}