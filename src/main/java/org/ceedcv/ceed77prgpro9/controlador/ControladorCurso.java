package org.ceedcv.ceed77prgpro9.controlador;

import java.awt.event.*;
import java.util.ArrayList;
import org.ceedcv.ceed77prgpro9.modelo.Curso;
import org.ceedcv.ceed77prgpro9.modelo.IModelo;
import org.ceedcv.ceed77prgpro9.vista.Funciones;
import org.ceedcv.ceed77prgpro9.vista.Verificador;
import org.ceedcv.ceed77prgpro9.vista.VistaGraficaCurso;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class ControladorCurso implements ActionListener, MouseListener {
    private IModelo modelo;
    private VistaGraficaCurso vista;
    private ArrayList cursos;
    private String chivato;
    private Curso actual;
    private int indice;
    private Funciones f = new Funciones();
    
    public ControladorCurso(IModelo m, VistaGraficaCurso v) {
        //Asignamos el modelo y la vista
        modelo = m;
        vista = v;
        
        //Añadimos los listeners
        vista.getSalir().addActionListener(this);
        vista.getGuardar().addActionListener(this);
        vista.getCancelar().addActionListener(this);
        vista.getCreate().addActionListener(this);
        vista.getUpdate().addActionListener(this);
        vista.getRead().addActionListener(this);
        vista.getDelete().addActionListener(this);
        vista.getSearch().addActionListener(this);
        vista.getPrimero().addActionListener(this);
        vista.getAnterior().addActionListener(this);
        vista.getSiguiente().addActionListener(this);
        vista.getUltimo().addActionListener(this);
        
        vista.getSalir().addMouseListener(this);
        vista.getGuardar().addMouseListener(this);
        vista.getCancelar().addMouseListener(this);
        vista.getCreate().addMouseListener(this);
        vista.getUpdate().addMouseListener(this);
        vista.getRead().addMouseListener(this);
        vista.getDelete().addMouseListener(this);
        vista.getSearch().addMouseListener(this);
        vista.getPrimero().addMouseListener(this);
        vista.getAnterior().addMouseListener(this);
        vista.getSiguiente().addMouseListener(this);
        vista.getUltimo().addMouseListener(this);
    }
    
    //Implementamos los métodos de las interfaces de eventos
    @Override
    public void actionPerformed(ActionEvent e) { 
        Object objeto = e.getSource();
        
        if (objeto == vista.getSalir()) {
            salir();
        } else if (objeto == vista.getCreate()) {
            create();
        } else if (objeto == vista.getRead()) {
            read();
        } else if (objeto == vista.getUpdate()) {
            update();
        } else if (objeto == vista.getDelete()) {
            delete();
        } else if (objeto == vista.getSearch()) {
            search();
        } else if (objeto == vista.getPrimero()) {
            primero();
        } else if (objeto == vista.getAnterior()) {
            anterior();
        } else if (objeto == vista.getSiguiente()) {
            siguiente();
        } else if (objeto == vista.getUltimo()) {
            ultimo();
        } else if (objeto == vista.getGuardar()) {
            guardar(chivato);
        } else if (objeto == vista.getCancelar()) {
            cancelar();
        }
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
        Object objeto = e.getSource();
        
        if (objeto == vista.getSalir()) {
            vista.getInfo().setText("Vuelve al menú principal");
        } else if (objeto == vista.getCreate()) {
            vista.getInfo().setText("Permite dar de alta un nuevo curso");        
        } else if (objeto == vista.getRead()) {
            vista.getInfo().setText("Permite navegar la lista de cursos");
        } else if (objeto == vista.getUpdate()) {
            vista.getInfo().setText("Permite actualizar los datos del curso");
        } else if (objeto == vista.getDelete()) {
            vista.getInfo().setText("Borra el curso en pantalla");
        } else if (objeto == vista.getSearch()) {
            vista.getInfo().setText("Busca a un curso a partir de su ID. Pulsa 'Guardar' para buscar. 'Cancelar' para salir");
        } else if (objeto == vista.getPrimero()) {
            vista.getInfo().setText("Muestra el primer curso de la lista");
        } else if (objeto == vista.getAnterior()) {
            vista.getInfo().setText("Muestra el curso anterior");
        } else if (objeto == vista.getSiguiente()) {
            vista.getInfo().setText("Muestra el curso siguiente");
        } else if (objeto == vista.getUltimo()) {
            vista.getInfo().setText("Muestra el último curso de la lista");
        } else if (objeto == vista.getGuardar()) {
            vista.getInfo().setText("Guarda la información");
        } else if (objeto == vista.getCancelar()) {
            vista.getInfo().setText("Cancela la operación en curso");
        }
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
        Object objeto = e.getSource();
        
        if (objeto == vista.getSalir() || objeto == vista.getCreate()
            || objeto == vista.getRead() || objeto == vista.getUpdate()
            || objeto == vista.getDelete() || objeto == vista.getSearch()
            || objeto == vista.getPrimero() || objeto == vista.getAnterior()
            || objeto == vista.getSiguiente() || objeto == vista.getUltimo()
            || objeto == vista.getGuardar() || objeto == vista.getCancelar()) {
                vista.getInfo().setText("MENÚ CURSO");
        }
    }
    
    public void mousePressed(MouseEvent e) {
        //No hacer nada
    }
    public void mouseReleased(MouseEvent e) {
        //No hacer nada
    }
    public void mouseClicked(MouseEvent e) {
        //No hacer nada
    }
    
    //Configuramos el comportamiento del menú CRUD
    private void create() {
        chivato = "create";
        vista.getTid().setEnabled(false);
        vista.getTnombre().setEnabled(true);
        vista.getThoras().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getRead().setEnabled(false);
        vista.getSearch().setEnabled(false);
        validaciones();
    }
    
    private void read() {
        cursos = modelo.readCurso();
        vista.getPrimero().setEnabled(true);
        vista.getAnterior().setEnabled(true);
        vista.getSiguiente().setEnabled(true);
        vista.getUltimo().setEnabled(true);
        vista.getCreate().setEnabled(false);
        vista.getRead().setEnabled(false);
        vista.getSearch().setEnabled(false);
        vista.getUpdate().setEnabled(true);
        vista.getDelete().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        primero();
    }
    
    private void update() {
        chivato = "update";
        vista.getTnombre().setEnabled(true);
        vista.getThoras().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getPrimero().setEnabled(false);
        vista.getAnterior().setEnabled(false);
        vista.getSiguiente().setEnabled(false);
        vista.getUltimo().setEnabled(false);
        validaciones();
    }
    
    private void delete() {
        Curso curso = alta();
        modelo.delete(curso);
        cancelar();
    }
    
    private void search() {
        chivato = "search";
        vista.getTid().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getCreate().setEnabled(false);
        vista.getRead().setEnabled(false);
    }
    
    //Configuramos el comportamiento del menú de navegabilidad
    private void primero() {
        if (cursos != null) {
            indice = 0;
            actual = ((Curso)cursos.get(indice));   
            vista.getTid().setText(actual.getIdCurso());
            vista.getTnombre().setText(actual.getNombre());
            vista.getThoras().setText(Integer.toString(actual.getHoras()));
        } else {
            actual = null;
            indice = -1;
        }
    }
        
    private void anterior()  {
        if (indice != 0)     {
            indice -= 1;
            actual = ((Curso)cursos.get(indice));
            vista.getTid().setText(actual.getIdCurso());
            vista.getTnombre().setText(actual.getNombre());
            vista.getThoras().setText(Integer.toString(actual.getHoras()));
        }
    }

    private void siguiente() {
        if (indice != cursos.size() - 1) {
            indice += 1;
            actual = ((Curso)cursos.get(indice));
            vista.getTid().setText(actual.getIdCurso());
            vista.getTnombre().setText(actual.getNombre());
            vista.getThoras().setText(Integer.toString(actual.getHoras()));;
        }
    }
  
    private void ultimo() {
        indice = (cursos.size() - 1);
        actual = ((Curso)cursos.get(indice));
        vista.getTid().setText(actual.getIdCurso());
        vista.getTnombre().setText(actual.getNombre());
        vista.getThoras().setText(Integer.toString(actual.getHoras()));
    }
    
    //Configuramos el comportamiento de los botones guardar, cancelar y salir
    private void guardar(String chivato) {
        Curso curso = new Curso();
        switch (chivato) {
            case "create":
                curso = alta();
                modelo.create(curso);
                cancelar();
                break;
            case "update":
                curso = alta();
                modelo.update(curso);
                cancelar();
                break;
            case "search":
                curso.setIdCurso(vista.getTid().getText());
                Curso c = modelo.search(curso);
                vista.getTnombre().setText(c.getNombre());
                vista.getThoras().setText(Integer.toString(c.getHoras()));
                break;
        }
    }
    
    private void cancelar() {
        //Deshabilitamos campos de texto 
        vista.getTid().setEnabled(false);
        vista.getTnombre().setEnabled(false);
        vista.getThoras().setEnabled(false);
        //Limpiamos campos de texto
        vista.getTid().setText("");
        vista.getTnombre().setText("");
        vista.getThoras().setText("");
        //Configuramos guardar, cancelar
        vista.getGuardar().setEnabled(false);
        vista.getCancelar().setEnabled(false);
        //Configuramos CRUD
        vista.getCreate().setEnabled(true);
        vista.getRead().setEnabled(true);
        vista.getUpdate().setEnabled(false);
        vista.getDelete().setEnabled(false);
        vista.getSearch().setEnabled(true);
        //Configuramos navegabilidad
        vista.getPrimero().setEnabled(false);
        vista.getAnterior().setEnabled(false);
        vista.getSiguiente().setEnabled(false);
        vista.getUltimo().setEnabled(false);
    }
    
    private void salir() {
        vista.dispose();
    }
    
    //Otras funciones
    private Curso alta() {
        String id = vista.getTid().getText();
        String nombre = vista.getTnombre().getText();
        int horas = Integer.parseInt(vista.getThoras().getText());
        Curso curso = new Curso(id, nombre, horas);
        return curso;
    }
    
    private void validaciones() {
        vista.getTnombre().setInputVerifier(new Verificador("null"));
        vista.getThoras().setInputVerifier(new Verificador("thoras"));
    }
}