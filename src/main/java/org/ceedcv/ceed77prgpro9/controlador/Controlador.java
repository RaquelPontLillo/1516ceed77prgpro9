package org.ceedcv.ceed77prgpro9.controlador;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.ceedcv.ceed77prgpro9.modelo.IModelo;
import org.ceedcv.ceed77prgpro9.modelo.ModeloDb4o;
import org.ceedcv.ceed77prgpro9.modelo.ModeloFichero;
import org.ceedcv.ceed77prgpro9.modelo.ModeloMysql;
import org.ceedcv.ceed77prgpro9.modelo.Usuario;
import org.ceedcv.ceed77prgpro9.vista.Funciones;
import org.ceedcv.ceed77prgpro9.vista.VistaCursoAlumno;
import org.ceedcv.ceed77prgpro9.vista.VistaGrafica;
import org.ceedcv.ceed77prgpro9.vista.VistaGraficaAcerca;
import org.ceedcv.ceed77prgpro9.vista.VistaGraficaAlumno;
import org.ceedcv.ceed77prgpro9.vista.VistaGraficaCurso;
import org.ceedcv.ceed77prgpro9.vista.VistaGraficaMatricula;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Controlador implements ActionListener {
    private IModelo modelo = null;
    private VistaGrafica vista = null;
    private Funciones f = new Funciones();
    private String s;
    
    public Controlador (IModelo m, VistaGrafica v) throws IOException {
        boolean valido = false;
        
        //Validamos el acceso
        while (!valido) {
            valido = validarAcceso();
            if (!valido) {
                f.error("El usuario o la contraseña no coinciden o son erróneos.");
            }
        }
        
        //Tomamos el modelo
        seleccionarModelo();
        
        //Asignamos el modelo y la vista
        vista = new VistaGrafica();
        
        //Centrar JFrame
        vista.setLocationRelativeTo(null);
        
        //Añadimos los listeners
        vista.getAlumno().addActionListener(this);
        vista.getAluCur().addActionListener(this);
        vista.getCurso().addActionListener(this);
        vista.getCurAlu().addActionListener(this);
        vista.getMatricula().addActionListener(this);
        vista.getCrearbbdd().addActionListener(this);
        vista.getCreartablas().addActionListener(this);
        vista.getCreardatos().addActionListener(this);
        vista.getBorrarbbdd().addActionListener(this);
        vista.getDocWeb().addActionListener(this);
        vista.getDocPdf().addActionListener(this);
        vista.getAcerca().addActionListener(this);
        vista.getSalir().addActionListener(this);
        
        //Seteamos la interfaz según el modelo
        switch (s) {
            case "db4o":
                vista.getCreardatos().setVisible(false);
                vista.getCreartablas().setVisible(false);
                break;
            case "mysql":
                vista.getCreardatos().setVisible(true);
                vista.getCreartablas().setVisible(true);
                break;
            case "csv":
                vista.getCreardatos().setVisible(false);
                vista.getCreartablas().setVisible(false);
                break;
        }
  }
    
    //Implementamos los métodos de las interfaces de eventos
    @Override
    public void actionPerformed(ActionEvent e) { 
        Object objeto = e.getSource();
        if (objeto == vista.getSalir()) {
            salir();
        } else if (objeto == vista.getAlumno()) {
            alumno();
        } else if (objeto == vista.getAluCur()) {
            aluCur();
        } else if (objeto == vista.getCurso()) {
            curso();
        } else if (objeto == vista.getCurAlu()) {
            curAlu();
        } else if (objeto == vista.getMatricula()) {
            matricula();
        } else if (objeto == vista.getCrearbbdd()) {
            crearBd();
        } else if (objeto == vista.getCreartablas()) {
            crearTablas();
        } else if (objeto == vista.getCreardatos()) {
            crearDatos();
        } else if (objeto == vista.getBorrarbbdd()) {
            eliminarBd();
        } else if (objeto == vista.getDocPdf()) {
            documentacionPdf();
        } else if (objeto == vista.getDocPdf()) {
            documentacionPdf();
        } else if (objeto == vista.getDocWeb()) {
            documentacionWeb();
        } else if (objeto == vista.getAcerca()) {
            acerca();
        }
    }
    
    public void mousePressed(MouseEvent e) {
        //No hacer nada
    }
    public void mouseReleased(MouseEvent e) {
        //No hacer nada
    }
    public void mouseClicked(MouseEvent e) {
        //No hacer nada
    }
    
    private void salir() {
        System.exit(0);
    }
    
    private void alumno() {
        VistaGraficaAlumno v = VistaGraficaAlumno.getInstancia();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            ex.printStackTrace();
        }
        ControladorAlumno ca = new ControladorAlumno(modelo, v);
    }
    
    private void aluCur() {
        VistaCursoAlumno v = VistaCursoAlumno.getInstancia();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            ex.printStackTrace();
        }  
        ControladorCurAlu cac = new ControladorCurAlu(modelo, vista, v, "alucur");
    }
    
    private void curso() {
        VistaGraficaCurso v = VistaGraficaCurso.getInstancia();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            ex.printStackTrace();
        }
        ControladorCurso cc = new ControladorCurso(modelo, v);    
    }
    
    
    private void curAlu() {
        VistaCursoAlumno v = VistaCursoAlumno.getInstancia();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            ex.printStackTrace();
        }  
        ControladorCurAlu cac = new ControladorCurAlu(modelo, vista, v, "curalu");
    }
    
    private void matricula() {
        VistaGraficaMatricula v = VistaGraficaMatricula.getInstancia();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        ControladorMatricula cm = new ControladorMatricula(modelo, v);
    }
    
    private void documentacionWeb() {
        String url = "https://docs.google.com/document/d/1pTpLNawKELyIV_Vn_pu5DMgM-HdIpmN13lkvVuf6X3o/edit?ts=5624d784";
        Desktop desktop = Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.getDesktop().browse(new URI(url));
            } catch (URISyntaxException | IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void documentacionPdf() {
        try {
            String ruta = "src/main/java/org/ceedcv/ceed77prgpro9/documentacion/documentacion.pdf";
            File pdf = new File(ruta);
            Desktop.getDesktop().open(pdf);
        } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Funciones de la base de datos
    private void crearBd() {
        try {
            modelo.crearBd();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void crearTablas() {
        try {
            modelo.crearTablas();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void crearDatos() {
        try {
            modelo.crearDatos();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void eliminarBd() {
        try {
            modelo.eliminarBd();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void acerca() {
        VistaGraficaAcerca v = VistaGraficaAcerca.getInstance();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    
    /* Otras funciones */
    private boolean validarAcceso() {
        Usuario u = new Usuario();
        String user = JOptionPane.showInputDialog(null, "Introduce el usuario (root)", "root");
        String pass = JOptionPane.showInputDialog(null, "Introduce la contraseña (pass)", "pass");
        return user.equals(u.getUsuario()) && pass.equals(u.getPassword());
    }
    
    private void seleccionarModelo() throws IOException {
        String[] modelos = {"CSV", "MYSQL" , "DB4O"};
        int r = JOptionPane.showOptionDialog(null, "Escoge sistema para guardar los datos",  "Escoger sistema", 0, JOptionPane.QUESTION_MESSAGE, null, modelos, modelos[0]);
        switch (r) {
            case 0:
                modelo = new ModeloFichero();
                s = "csv";
                break;
            case 1:
                modelo = new ModeloMysql();
                s = "mysql";
                break;
            case 2:
                modelo = new ModeloDb4o();
                s = "db4o";
                break;               
        }
    }
}
