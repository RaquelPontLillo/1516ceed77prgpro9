package org.ceedcv.ceed77prgpro9.controlador;

import java.awt.event.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;
import org.ceedcv.ceed77prgpro9.modelo.Alumno;
import org.ceedcv.ceed77prgpro9.modelo.Curso;
import org.ceedcv.ceed77prgpro9.modelo.IModelo;
import org.ceedcv.ceed77prgpro9.modelo.Matricula;
import org.ceedcv.ceed77prgpro9.vista.Funciones;
import org.ceedcv.ceed77prgpro9.vista.Verificador;
import org.ceedcv.ceed77prgpro9.vista.VistaGraficaMatricula;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class ControladorMatricula implements ActionListener, MouseListener, ItemListener {
    private IModelo modelo = null;
    private VistaGraficaMatricula vista;
    private ArrayList alumnos;
    private ArrayList cursos;
    private ArrayList matriculas;
    private String chivato;
    private Matricula actual;
    private int indice;
    
    public ControladorMatricula(IModelo m, VistaGraficaMatricula v) {
        modelo = m;
        vista = v;
        cargaCombo();
        
        //Añadimos los listeners
        vista.getSalir().addActionListener(this);
        vista.getGuardar().addActionListener(this);
        vista.getCancelar().addActionListener(this);
        vista.getCreate().addActionListener(this);
        vista.getUpdate().addActionListener(this);
        vista.getRead().addActionListener(this);
        vista.getDelete().addActionListener(this);
        vista.getSearch().addActionListener(this);
        vista.getPrimero().addActionListener(this);
        vista.getAnterior().addActionListener(this);
        vista.getSiguiente().addActionListener(this);
        vista.getUltimo().addActionListener(this);
        
        vista.getSalir().addMouseListener(this);
        vista.getGuardar().addMouseListener(this);
        vista.getCancelar().addMouseListener(this);
        vista.getCreate().addMouseListener(this);
        vista.getUpdate().addMouseListener(this);
        vista.getRead().addMouseListener(this);
        vista.getDelete().addMouseListener(this);
        vista.getSearch().addMouseListener(this);
        vista.getPrimero().addMouseListener(this);
        vista.getAnterior().addMouseListener(this);
        vista.getSiguiente().addMouseListener(this);
        vista.getUltimo().addMouseListener(this);
        
        vista.getCbAlumno().addItemListener(this);
        vista.getCbCurso().addItemListener(this);

    }
    
        //Implementamos los métodos de las interfaces de eventos
    @Override
    public void actionPerformed(ActionEvent e) { 
        Object objeto = e.getSource();
        
        if (objeto == vista.getSalir()) {
            salir();
        } else if (objeto == vista.getCreate()) {
            create();
        } else if (objeto == vista.getRead()) {
            read();
        } else if (objeto == vista.getUpdate()) {
            update();
        } else if (objeto == vista.getDelete()) {
            delete();
        } else if (objeto == vista.getSearch()) {
            search();
        } else if (objeto == vista.getPrimero()) {
            primero();
        } else if (objeto == vista.getAnterior()) {
            anterior();
        } else if (objeto == vista.getSiguiente()) {
            siguiente();
        } else if (objeto == vista.getUltimo()) {
            ultimo();
        } else if (objeto == vista.getGuardar()) {
            guardar(chivato);
        } else if (objeto == vista.getCancelar()) {
            cancelar();
        }
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
        Object objeto = e.getSource();
        
        if (objeto == vista.getSalir()) {
            vista.getInfo().setText("Vuelve al menú principal");
        } else if (objeto == vista.getCreate()) {
            vista.getInfo().setText("Permite dar de alta una matrícula");        
        } else if (objeto == vista.getRead()) {
            vista.getInfo().setText("Permite navegar la lista de matrículas");
        } else if (objeto == vista.getUpdate()) {
            vista.getInfo().setText("Permite actualizar los datos de la matrícula");
        } else if (objeto == vista.getDelete()) {
            vista.getInfo().setText("Borra la matrícula en pantalla");
        } else if (objeto == vista.getSearch()) {
            vista.getInfo().setText("Busca por ID. 'Guardar' para buscar. 'Cancelar' para salir");
        } else if (objeto == vista.getPrimero()) {
            vista.getInfo().setText("Muestra la primera matrícula de la lista");
        } else if (objeto == vista.getAnterior()) {
            vista.getInfo().setText("Muestra la matrícula anterior");
        } else if (objeto == vista.getSiguiente()) {
            vista.getInfo().setText("Muestra la matrícula siguiente");
        } else if (objeto == vista.getUltimo()) {
            vista.getInfo().setText("Muestra la última matrícula de la lista");
        } else if (objeto == vista.getGuardar()) {
            vista.getInfo().setText("Guarda la información");
        } else if (objeto == vista.getCancelar()) {
            vista.getInfo().setText("Cancela la operación en curso");
        }
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
        Object objeto = e.getSource();
        
        if (objeto == vista.getSalir() || objeto == vista.getCreate()
            || objeto == vista.getRead() || objeto == vista.getUpdate()
            || objeto == vista.getDelete() || objeto == vista.getSearch()
            || objeto == vista.getPrimero() || objeto == vista.getAnterior()
            || objeto == vista.getSiguiente() || objeto == vista.getUltimo()
            || objeto == vista.getGuardar() || objeto == vista.getCancelar()) {
                vista.getInfo().setText("MENÚ MATRÍCULA");
        }
    }
    
    public void mousePressed(MouseEvent e) {
        //No hacer nada
    }
    public void mouseReleased(MouseEvent e) {
        //No hacer nada
    }
    public void mouseClicked(MouseEvent e) {
        //No hacer nada
    }
    
    public void itemStateChanged(ItemEvent e) {
        Object objeto = e.getSource();  
        if (objeto == vista.getCbAlumno()) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                String combo = (String)vista.getCbAlumno().getSelectedItem();
                StringTokenizer stokenizer = new StringTokenizer(combo, " - ");
                String id = stokenizer.nextToken();
                Iterator it = alumnos.iterator(); 
                while (it.hasNext()) { 
                    Alumno alumno = (Alumno) it.next(); 
                    if (alumno.getId().equals(id)) {
                        vista.getIdA().setText(alumno.getId());
                        vista.getTdni().setText(alumno.getDni());
                        vista.getTedad().setText(Integer.toString(alumno.getEdad()));
                        vista.getTemail().setText(alumno.getEmail());
                    }
                }
            }
        } else if (objeto == vista.getCbCurso()) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                String combo = (String)vista.getCbCurso().getSelectedItem();
                StringTokenizer stokenizer = new StringTokenizer(combo, " - ");
                String id = stokenizer.nextToken();
                cursos = modelo.readCurso();
                Iterator it = cursos.iterator(); 
                while (it.hasNext()) { 
                    Curso curso = (Curso) it.next(); 
                    if (curso.getIdCurso().equals(id)) {
                        vista.getIdC().setText(curso.getIdCurso());
                        vista.getThoras().setText(Integer.toString(curso.getHoras()));
                    }
                }
            }
        }
    }
    
    //Configuramos el comportamiento del menú CRUD    
    private void create() {
        chivato = "create";
        vista.getIdM().setEnabled(false);
        vista.getTfecha().setEnabled(true);
        vista.getCbAlumno().setEnabled(true);
        vista.getCbCurso().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getRead().setEnabled(false);
        vista.getSearch().setEnabled(false);
    }
    
    private void read() {   
        matriculas = modelo.readMatricula();
        vista.getPrimero().setEnabled(true);
        vista.getAnterior().setEnabled(true);
        vista.getSiguiente().setEnabled(true);
        vista.getUltimo().setEnabled(true);
        vista.getCreate().setEnabled(false);
        vista.getRead().setEnabled(false);
        vista.getSearch().setEnabled(false);
        vista.getUpdate().setEnabled(true);
        vista.getDelete().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        primero();
    }
    
    private void update() {
        chivato = "update";
        vista.getCbAlumno().setEnabled(true);
        vista.getCbCurso().setEnabled(true);
        vista.getTfecha().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getPrimero().setEnabled(false);
        vista.getAnterior().setEnabled(false);
        vista.getSiguiente().setEnabled(false);
        vista.getUltimo().setEnabled(false);
    }
    
    private void delete() {      
        Matricula matricula = alta();
        modelo.delete(matricula);
        cancelar();
    }
    
    private void search() {
        chivato = "search";
        vista.getIdM().setEnabled(true);
        vista.getGuardar().setEnabled(true);
        vista.getCancelar().setEnabled(true);
        vista.getCreate().setEnabled(false);
        vista.getRead().setEnabled(false);
    }
    
    //Configuramos el comportamiento del menú de navegabilidad
    private void primero() {
        if (matriculas != null) {
            indice = 0;
            actual = ((Matricula)matriculas.get(indice));   
            vista.getIdM().setText(actual.getIdMatricula());
            vista.getTfecha().setDate(actual.getFecha());
            vista.getIdA().setText(actual.getAlumno().getId());
            cargaAlumno();
            vista.getIdC().setText(actual.getCurso().getIdCurso());
            cargaCurso();
        } else {
            actual = null;
            indice = -1;
        }
    }
        
    private void anterior()  {
        if (indice != 0) {
            indice -= 1;
            actual = ((Matricula)matriculas.get(indice));
            vista.getIdM().setText(actual.getIdMatricula());
            vista.getTfecha().setDate(actual.getFecha());
            vista.getIdA().setText(actual.getAlumno().getId());
            cargaAlumno();
            vista.getIdC().setText(actual.getCurso().getIdCurso());
            cargaCurso();
        }
    }

    private void siguiente() {
        if (indice != matriculas.size() - 1) {
            indice += 1;
            actual = ((Matricula)matriculas.get(indice));
            vista.getIdM().setText(actual.getIdMatricula());
            vista.getTfecha().setDate(actual.getFecha());
            vista.getIdA().setText(actual.getAlumno().getId());
            cargaAlumno();
            vista.getIdC().setText(actual.getCurso().getIdCurso());
            cargaCurso();
        }
    }
  
    private void ultimo() {
        indice = (matriculas.size() - 1);
        actual = ((Matricula)matriculas.get(indice));
        vista.getIdM().setText(actual.getIdMatricula());
        vista.getTfecha().setDate(actual.getFecha());
        vista.getIdA().setText(actual.getAlumno().getId());
        cargaAlumno();
        vista.getIdC().setText(actual.getCurso().getIdCurso());
        cargaCurso();
    }
    
    //Configuramos el comportamiento de los botones guardar, cancelar y salir
    private void guardar(String chivato) {
        Matricula matricula = new Matricula();
        Funciones f = new Funciones();
        switch (chivato) {
            case "create":
                try {
                    matricula = alta();
                    if (matricula.getFecha() == null) {
                        vista.getTfecha().setDate(new Date());
                        matricula.setFecha(new Date());
                    }
                    modelo.create(matricula);
                    cancelar();
                } catch (Exception e) {
                    f.error("Ha ocurrido un error.\n"
                            + "Error: " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            case "update":
                try {
                    matricula = alta();
                    modelo.update(matricula);
                    cancelar();
                } catch (Exception e) {
                    f.error("Ha ocurrido un error.\n"
                            + "Error: " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            case "search":
                try {
                    matricula.setIdMatricula(vista.getIdM().getText());
                    Matricula m = modelo.search(matricula);
                    vista.getTfecha().setDate(m.getFecha());
                    vista.getIdA().setText(m.getAlumno().getId());
                    cargaAlumno();
                    vista.getIdC().setText(m.getCurso().getIdCurso());
                    cargaCurso();
                } catch (Exception e) {
                    f.error("Ha ocurrido un error.\n"
                            + "Error: " + e.getMessage());
                    e.printStackTrace();
                }
                break;
        }
    }
    
    private void cancelar() {
        //Deshabilitamos campos de texto 
        vista.getIdM().setEnabled(false);
        vista.getTfecha().setEnabled(false);
        vista.getIdA().setEnabled(false);
        vista.getCbAlumno().setEnabled(false);
        vista.getTdni().setEnabled(false);
        vista.getTedad().setEnabled(false);
        vista.getTemail().setEnabled(false);
        vista.getIdC().setEnabled(false);
        vista.getCbCurso().setEnabled(false);
        vista.getThoras().setEnabled(false);
        //Limpiamos campos de texto
        vista.getIdM().setText("");
        vista.getTfecha().setDate(null);
        vista.getIdA().setText("");
        vista.getCbAlumno().setSelectedItem(null);
        vista.getTdni().setText("");
        vista.getTedad().setText("");
        vista.getTemail().setText("");
        vista.getIdC().setText("");
        vista.getCbCurso().setSelectedItem(null);
        vista.getThoras().setText("");
        //Configuramos guardar, cancelar
        vista.getGuardar().setEnabled(false);
        vista.getCancelar().setEnabled(false);
        //Configuramos CRUD
        vista.getCreate().setEnabled(true);
        vista.getRead().setEnabled(true);
        vista.getUpdate().setEnabled(false);
        vista.getDelete().setEnabled(false);
        vista.getSearch().setEnabled(true);
        //Configuramos navegabilidad
        vista.getPrimero().setEnabled(false);
        vista.getAnterior().setEnabled(false);
        vista.getSiguiente().setEnabled(false);
        vista.getUltimo().setEnabled(false);
    }
    
    private void salir() {
        vista.dispose();
    }
    
    //Otras funciones
    private Matricula alta() {
        String nombre = "";
        String nombreCurso = "";
        //Datos matrícula
        String idM = vista.getIdM().getText();
        Date fecha = vista.getTfecha().getDate();
        //Datos alumno
        String idA = vista.getIdA().getText();
        Iterator it = (modelo.readAlumno()).iterator();
        while (it.hasNext()) {
            Alumno a = (Alumno) it.next();
            if (a.getId().equals(idA)) {
                nombre = a.getNombre();
            }
        }
        String dni = vista.getTdni().getText();
        int edad = Integer.parseInt(vista.getTedad().getText());
        String email = vista.getTemail().getText();
        //Datos curso
        String idC = vista.getIdC().getText();
        it = (modelo.readCurso()).iterator();
        while (it.hasNext()) {
            Curso c = (Curso) it.next();
            if (c.getIdCurso().equals(idC)) {
                nombreCurso = c.getNombre();
            }
        }
        int horas = Integer.parseInt(vista.getThoras().getText());
        Alumno alumno = new Alumno(idA, nombre, dni, edad, email);
        Curso curso = new Curso(idC, nombreCurso, horas);
        Matricula matricula = new Matricula(idM, fecha, curso, alumno);
        return matricula;
    }
    
    private void cargaCombo() {
        alumnos = null;
        alumnos = modelo.readAlumno();
        Iterator it = alumnos.iterator(); 
        vista.getCbAlumno().removeAllItems();
        vista.getCbAlumno().addItem(null);
        while (it.hasNext()) { 
            Alumno alumno = (Alumno) it.next();  
            String idAlumno = alumno.getId();
            String nombre = alumno.getNombre();
            vista.getCbAlumno().addItem(idAlumno + " - " + nombre);
        }
        cursos = null;
        cursos = modelo.readCurso();
        vista.getCbCurso().removeAllItems();
        vista.getCbCurso().addItem(null);
        it = cursos.iterator(); 
        while (it.hasNext()) { 
            Curso curso = (Curso) it.next(); 
            String idCurso = curso.getIdCurso();
            String nombreCurso = curso.getNombre();
            vista.getCbCurso().addItem(idCurso + " - " + nombreCurso);
        }
    }
    
    private void cargaAlumno() {
        String id;
        alumnos = modelo.readAlumno();
        Iterator it;
        Alumno alumno;
        if (!vista.getIdA().getText().equals("")) {
            id = vista.getIdA().getText();
            it = alumnos.iterator(); 
            while (it.hasNext()) { 
                alumno = (Alumno) it.next(); 
                if (alumno.getId().equals(id)) {
                    vista.getIdA().setText(alumno.getId());
                    vista.getCbAlumno().setSelectedItem(alumno.getId() + " - " + alumno.getNombre());
                    vista.getTdni().setText(alumno.getDni());
                    vista.getTedad().setText(Integer.toString(alumno.getEdad()));
                    vista.getTemail().setText(alumno.getEmail());
                }
            }          
        } else if (!vista.getCbAlumno().getSelectedItem().equals("")) {
            String combo = (String)vista.getCbAlumno().getSelectedItem();
            StringTokenizer stokenizer = new StringTokenizer(combo, " - ");
            id = stokenizer.nextToken();
            it = alumnos.iterator(); 
            while (it.hasNext()) { 
                alumno = (Alumno) it.next(); 
                if (alumno.getId().equals(id)) {
                    vista.getIdA().setText(alumno.getId());
                    vista.getTdni().setText(alumno.getDni());
                    vista.getTedad().setText(Integer.toString(alumno.getEdad()));
                    vista.getTemail().setText(alumno.getEmail());
                }
            }
        }
    }
    
    private void cargaCurso() {
        String id;
        cursos = modelo.readCurso();
        Iterator it;
        Curso curso;
        
        if (!vista.getIdC().getText().equals("")) {
            id = vista.getIdC().getText();
            it = cursos.iterator();
            while (it.hasNext()) { 
                curso = (Curso) it.next(); 
                if (curso.getIdCurso().equals(id)) {
                    vista.getIdC().setText(curso.getIdCurso());
                    vista.getCbCurso().setSelectedItem(curso.getIdCurso() + " - " + curso.getNombre());
                    vista.getThoras().setText(Integer.toString(curso.getHoras()));
                }
            }
        } else if (!vista.getCbCurso().getSelectedItem().equals("")) {
            String combo = (String)vista.getCbCurso().getSelectedItem();
            StringTokenizer stokenizer = new StringTokenizer(combo, " - ");
            id = stokenizer.nextToken();
            cursos = modelo.readCurso();
            it = cursos.iterator(); 
            while (it.hasNext()) { 
                curso = (Curso) it.next(); 
                if (curso.getIdCurso().equals(id)) {
                    vista.getIdC().setText(curso.getIdCurso());
                    vista.getThoras().setText(Integer.toString(curso.getHoras()));
                }
            }
        }
    }
}