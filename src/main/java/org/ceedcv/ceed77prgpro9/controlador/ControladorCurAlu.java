package org.ceedcv.ceed77prgpro9.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import org.ceedcv.ceed77prgpro9.modelo.Alumno;
import org.ceedcv.ceed77prgpro9.modelo.Curso;
import org.ceedcv.ceed77prgpro9.modelo.IModelo;
import org.ceedcv.ceed77prgpro9.modelo.Matricula;
import org.ceedcv.ceed77prgpro9.vista.VistaCursoAlumno;
import org.ceedcv.ceed77prgpro9.vista.VistaGrafica;
import org.ceedcv.ceed77prgpro9.vista.VistaGraficaAlumno;
import org.ceedcv.ceed77prgpro9.vista.VistaGraficaCurso;
import org.ceedcv.ceed77prgpro9.vista.VistaGraficaMatricula;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class ControladorCurAlu implements ActionListener, MouseListener {
    private static final SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private VistaGrafica vista;
    private VistaCursoAlumno vca;
    private IModelo modelo;
    private String chivato;
    private ArrayList alumnos, cursos, matriculas;
        
    public ControladorCurAlu(IModelo m, VistaGrafica vista, VistaCursoAlumno v, String s) {
        modelo = m;
        this.vista = vista;
        vca = v;
        chivato = s;
        
        //Seleccionamos la pestaña activa, según el menú desde el que accedemos
        switch (chivato) {
            case "alucur":
                vca.getTabs().setSelectedIndex(0);
                break;
            case "curalu":
                vca.getTabs().setSelectedIndex(1);
                break;
        }
        
        //Añadimos los listeners
        vca.getAlumno().addActionListener(this);
        vca.getAlumno2().addActionListener(this);
        vca.getCurso().addActionListener(this);
        vca.getCurso2().addActionListener(this);
        vca.getMatricula().addActionListener(this);
        vca.getMatricula2().addActionListener(this);
        vca.getRefrescar().addActionListener(this);
        vca.getRefrescar2().addActionListener(this);
        vca.getSalir().addActionListener(this);
        vca.getSalir2().addActionListener(this);
        vca.gettCursos().addMouseListener(this);
        vca.gettAlumnos2().addMouseListener(this);
        
        //Llenamos las tablas
        cargarTablas();
    }

    //Implementamos los métodos de las interfaces de eventos
    @Override
    public void actionPerformed(ActionEvent e) {
        Object objeto = e.getSource();
        if (objeto == vca.getSalir() || objeto == vca.getSalir2()) {
            salir();
        } else if (objeto == vca.getAlumno() || objeto == vca.getAlumno2()) {
            alumno();
        } else if (objeto == vca.getCurso() || objeto == vca.getCurso2()) {
            curso();
        } else if (objeto == vca.getMatricula() || objeto == vca.getMatricula2()) {
            matricula();
        } else if (objeto == vca.getRefrescar() || objeto == vca.getRefrescar2()) {
            refrescar();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Object objeto = e.getSource();
        if (objeto == vca.gettCursos()) {
            porCurso(e);
        }
        if (objeto == vca.gettAlumnos2()) {
            porAlumno(e);
        }
    }
    @Override
    public void mousePressed(MouseEvent e) {
        //No hacer nada
    }
    @Override
    public void mouseReleased(MouseEvent e) {
        //No hacer nada
    }
    @Override
    public void mouseEntered(MouseEvent e) {
        //No hacer nada
    }
    @Override
    public void mouseExited(MouseEvent e) {
        //No hacer nada
    }
    
    /* Funciones de los botones */
    private void salir() {
        vca.dispose();
    }
    
    private void refrescar() {
        cargarTablas();
    }
    
    private void alumno() {
        VistaGraficaAlumno v = VistaGraficaAlumno.getInstancia();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        ControladorAlumno ca = new ControladorAlumno(modelo, v);
    }
    
    private void curso() {
        VistaGraficaCurso v = VistaGraficaCurso.getInstancia();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        ControladorCurso cc = new ControladorCurso(modelo, v); 
    }
    
    private void matricula() {
        VistaGraficaMatricula v = VistaGraficaMatricula.getInstancia();
        v.pack();        
        if (!v.isVisible()) {
            vista.getEscritorio().add(v);
            v.setVisible(true);
        }     
        try {
            v.setMaximum(true);
            v.setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        ControladorMatricula cm = new ControladorMatricula(modelo, v);
    }
    
    /* Funciones de las tablas */
    //Llenado de la tabla de cursos (pestaña alumnos por curso)
    private void tablaCursos(ArrayList cursos) {
        if (!cursos.isEmpty()) {
            vca.gettCursos().setVisible(true);
            String[] curso = { "ID", "Nombre", "Horas" };
      
            String[] tablas = new String[curso.length];
            DefaultTableModel dtm = new DefaultTableModel((Object[][])null, curso);
      
            Iterator it = cursos.iterator();
            while (it.hasNext()) {
                Curso c = (Curso)it.next();
                tablas[0] = c.getIdCurso();
                tablas[1] = c.getNombre();
                tablas[2] = Integer.toString(c.getHoras());
                dtm.addRow(tablas);
            }
            vca.gettCursos().setModel(dtm);
        } else {
            vca.gettCursos().setVisible(false);
        }
    }
    
    //Llenado de la tabla de alumnos (pestaña alumnos por curso)
    private void tablaAlumnos(ArrayList matriculas, Curso curso) {
        if (!matriculas.isEmpty()) {
            vca.gettAlumnos().setVisible(true);
            String[] alumnos = { "ID", "Nombre", "DNI/NIE", "Edad", "Email", "Fecha de alta" };
      
            String[] tablas = new String[alumnos.length];
            DefaultTableModel dtm = new DefaultTableModel((Object[][])null, alumnos);
      
            Iterator it = matriculas.iterator();
            while (it.hasNext()) {
                Matricula m = (Matricula)it.next();
                if (m.getCurso().getIdCurso().equals(curso.getIdCurso())) {
                    tablas[0] = m.getAlumno().getId();
                    tablas[1] = m.getAlumno().getNombre();
                    tablas[2] = m.getAlumno().getDni();
                    tablas[3] = Integer.toString(m.getAlumno().getEdad());
                    tablas[4] = m.getAlumno().getEmail();
                    tablas[5] = SHORT_DATE_FORMAT.format(m.getFecha());
                    dtm.addRow(tablas);
                }
            }
            vca.gettAlumnos().setModel(dtm);
        } else {
            vca.gettAlumnos().setVisible(false);
        }
    }
    
    //Llenado de la tabla alumnos (pestaña cursos por alumno)
    private void tablaAluCur(ArrayList alumnos) {
        if (!alumnos.isEmpty()) {
            vca.gettAlumnos2().setVisible(true);
            String[] alumno = { "ID", "Nombre", "DNI/NIE", "Edad", "Email" };

            String[] tablas = new String[alumno.length];
            DefaultTableModel dtm = new DefaultTableModel((Object[][])null, alumno);

            Iterator it = alumnos.iterator();
            while (it.hasNext()) {
                Alumno a = (Alumno)it.next();
                tablas[0] = a.getId();
                tablas[1] = a.getNombre();
                tablas[2] = a.getDni();
                tablas[3] = Integer.toString(a.getEdad());
                tablas[4] = a.getEmail();
                dtm.addRow(tablas);
            }
            vca.gettAlumnos2().setModel(dtm);
        } else {
            vca.gettAlumnos2().setVisible(false);
        }
    }
    
    //Llenado de la tabla de cursos (pestaña cursos por alumno)
    private void tablaCurAlu(ArrayList matriculas, Alumno alumno) {
        if (!matriculas.isEmpty()) {
            vca.gettCursos2().setVisible(true);
            String[] curso = { "ID", "Nombre", "Horas", "Fecha de alta" };
      
            String[] tablas = new String[curso.length];
            DefaultTableModel dtm = new DefaultTableModel((Object[][])null, curso);
      
            Iterator it = matriculas.iterator();
            while (it.hasNext()) {
                Matricula m = (Matricula)it.next();
                if (m.getAlumno().getId().equals(alumno.getId())) {
                    tablas[0] = m.getCurso().getIdCurso();
                    tablas[1] = m.getCurso().getNombre();
                    tablas[2] = Integer.toString(m.getCurso().getHoras());
                    tablas[3] = SHORT_DATE_FORMAT.format(m.getFecha());
                    dtm.addRow(tablas);
                }
            }
            vca.gettCursos2().setModel(dtm);
        } else {
            vca.gettCursos2().setVisible(false);
        }   
    }
    
    //Llenado de las tablas 
    private void cargarTablas() {
        alumnos = modelo.readAlumno();
        cursos = modelo.readCurso();
        matriculas = modelo.readMatricula();
        tablaCursos(cursos);
        if (cursos != null) {
            Curso c = ((Curso)cursos.get(0));
            tablaAlumnos(matriculas, c);
        }
        tablaAluCur(alumnos);
        if (alumnos != null) {
            Alumno a = ((Alumno)alumnos.get(0));
            tablaCurAlu(matriculas, a);
        }
    }
    
    /* Funciones de llenado de tablas al hacer click en la principal */
    private void porCurso(MouseEvent e) {
        Curso curso = null;
        matriculas = modelo.readMatricula();
        int row = vca.gettCursos().rowAtPoint(e.getPoint());
        if (row != -1) {
            String id = (String)vca.gettCursos().getValueAt(row, 0);
            for (Object c: cursos) {
                curso = (Curso) c;
                if (curso.getIdCurso().equals(id)) {
                    break;
                }
            }
        }
        if (curso != null) {    
            tablaAlumnos(matriculas, curso);
        }
    }
    
    private void porAlumno(MouseEvent e) {
        Alumno alumno = null;
        matriculas = modelo.readMatricula();
        int row = vca.gettAlumnos2().rowAtPoint(e.getPoint());
        if (row != -1) {
            String id = (String)vca.gettAlumnos2().getValueAt(row, 0);
            for (Object a : alumnos) {
                alumno = (Alumno) a;
                if (alumno.getId().equals(id)) {
                    break;
                }
            }
        }
        if (alumno != null) {    
            tablaCurAlu(matriculas, alumno);
        }
    }
}