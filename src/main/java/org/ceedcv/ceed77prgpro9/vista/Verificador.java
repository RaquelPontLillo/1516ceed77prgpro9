package org.ceedcv.ceed77prgpro9.vista;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.*;


/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Verificador extends InputVerifier {
    private String variable;
    private Funciones f = new Funciones();
  
    public Verificador(String var) {
        variable = var;
    }

    private boolean validarEmail(String s) {
        Pattern pattern = Pattern.compile("[\\w\\.\\d]+@\\w+\\.\\w+");
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }

    private boolean validarDNI (String s) {
        Pattern pattern = Pattern.compile("(([X-Z]{1})([-])(\\d{7})([-])([A-Z]{1}))|((\\d{8})([-])([A-Z]{1}))");
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }

    private boolean validarLetra (String s) { 
        boolean dni = validarDNI(s);
        if (dni) {
            char letras[] = {'T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'};
            if (s.length() < 11) {
                char letra = s.charAt(9);
                int numeros = Integer.parseInt(s.substring(0,8));
                return letra == letras[numeros%23];
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    private boolean validarNumero(String texto) {
        boolean valido = true;
        try {
            int numero = Integer.parseInt(texto);
        } catch (Exception e) {
            int numero;
            valido = false;
        }
        return valido;
    }

    public boolean verify(JComponent input) {
        String texto = null;
        if (input instanceof JTextField) {
            texto = ((JTextField)input).getText();
            switch (variable) {
            case "tedad": 
                if (!validarNumero(texto)) {
                    f.error("El valor introducido para la edad no es numérico.");
                    return false;
                }
                break;
            case "temail": 
              if (!validarEmail(texto)) {
                  f.error("El e-mail no sigue un formato válido (ej: nombre@empresa.com)");
                  return false;
              }
                  break;
            case "tdni":
                if (!validarLetra(texto)) {     
                    f.error("El DNI o NIE no sigue un formato válido o la letra no es correcta (ej: DNI: 11223344-A / NIE: X-1223344-A)");
                    return false;
                }
                break;
            case "null":
                    if ("".equals(texto)) { 
                        f.error("El campo nombre no puede estar vacío.");
                        return false;
                    }
                    break;
            case "thoras":
                if (!validarNumero(texto)) {
                    f.error("El valor introducido para las horas no es numérico.");
                    return false;
                }
                break;
            }   
        }
        return true;
    }
}