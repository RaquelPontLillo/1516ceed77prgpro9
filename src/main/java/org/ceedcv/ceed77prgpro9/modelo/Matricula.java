package org.ceedcv.ceed77prgpro9.modelo;

import java.util.Date;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Matricula {
  //Variables de la clase
  private String idMatricula;
  private Curso curso;
  private Alumno alumno;
  private Date fecha;
  
  //Constructores de la clase
  
  public Matricula (String idM) {
    idMatricula = idM;
  }
  
  public Matricula() {
      
  }
  
  public Matricula(String idM, Date fec, Curso cur, Alumno al) {
      idMatricula = idM;
      fecha = fec;
      curso = cur;
      alumno = al;
  }
  
  //Métodos de la clase
 
  //Getters
 public String getIdMatricula() {
    return idMatricula;
 }
 
 public Curso getCurso() {
    return curso;
 }
 
 public Alumno getAlumno() {
    return alumno;
 }
 
 public Date getFecha() {
     return fecha;
 }
 
 //Setters
 public void setIdMatricula (String idM){
    idMatricula = idM;
 }
 
 public void setCurso (Curso idC){
    curso = idC;
 }
 
 public void setAlumno (Alumno idA){
    alumno = idA;
 }
 
 public void setFecha (Date fec) {
     fecha = fec;
 }
 
}