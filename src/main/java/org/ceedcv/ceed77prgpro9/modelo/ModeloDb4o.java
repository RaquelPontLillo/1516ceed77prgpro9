package org.ceedcv.ceed77prgpro9.modelo;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.ceedcv.ceed77prgpro9.vista.Funciones;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class ModeloDb4o implements IModelo {
    private static final SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private String bbddoo = "bbddoo.db4o";
    private File existe = new File(bbddoo);
    private ObjectContainer bbdd;
    private ObjectSet os;
    private Funciones f = new Funciones();
    private int idA=0, idC=0, idM=0;
    private ArrayList alumnos, cursos, matriculas;
    private Alumno alumno;
    private Curso curso;
    private Matricula matricula;
    
    public ModeloDb4o() {
        if (!existe.exists()) {
            f.error("No existe la base de datos orientada a objetos. \n"
                    + "Deberás instalarla previamente para usar el programa.");
        }
    }
    
    @Override
    public void conectarBd() {
        bbdd = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), bbddoo);
        System.out.println("Base de datos orientada a objetos conectada.");
    }
    
    @Override
    public void desconectarBd() {
        bbdd.close();
        System.out.println("Base de datos orientada a objetos desconectada.");
    }
    
    public void crearBd() {
        try {
            Alumno alumno1 = new Alumno("0", "Juan", "44522840-S", 25, "juan@gmail.com");
            Alumno alumno2 = new Alumno("1", "Luis", "44522840-S", 36, "luis@gmail.com");
            Curso curso1 = new Curso("0", "Matemáticas", 40);
            Curso curso2 = new Curso("1", "Lengua", 80);
            create(alumno1);
            create(alumno2);
            create(curso1);
            create(curso2);
            Matricula matricula1 = new Matricula("0", SHORT_DATE_FORMAT.parse("2016-03-12"), curso1, alumno1);
            Matricula matricula2 = new Matricula("1", SHORT_DATE_FORMAT.parse("2016-03-26"), curso2, alumno2);
            create(matricula1);
            create(matricula2);
            f.info("Base de datos orientada a objetos creada correctamente.");
        } catch (ParseException pe) {
            pe.printStackTrace();
            f.error("Ha ocurrido un error durante la instalación."
                    + "Error: " + pe.getMessage());
        }
    }
    
    @Override
    public void eliminarBd() {
        try {
            idA = 0;
            idC = 0;
            idM = 0;
            existe.delete();
            f.info("Base de datos " + bbddoo + "eliminada correctamente.");
        } catch (Exception e) {
            e.printStackTrace();
            f.error("Ha ocurrido un error durante la desinstalación."
                    + "Error: " + e.getMessage());
        }
    }
    
    //Funciones CRUD alumno, curso y matrícula
    @Override
    public void create(Alumno alumno) {
        if (existe.exists()) {
            idA = idAlumno();
        }
        conectarBd();
        alumno.setId(Integer.toString(idA));
        idA++;
        bbdd.store(alumno);
        desconectarBd();
        System.out.println("Alumno " + alumno.getNombre() + " creado correctamente.");
        f.info("Alumno " + alumno.getNombre() + " creado correctamente.");
    }    
    
    @Override
    public void create(Curso curso) {
        if (existe.exists()) {
            idC = idCurso();
        }
        conectarBd();
        curso.setIdCurso(Integer.toString(idC));
        idC++;
        bbdd.store(curso);
        desconectarBd();
        System.out.println("Curso " + curso.getNombre() + " creado correctamente.");
        f.info("Curso " + curso.getNombre() + " creado correctamente.");
    }
        
    @Override
    public void create(Matricula matricula) {
        if (existe.exists()) {
            idM = idMatricula();
        }
        conectarBd();
        matricula.setIdMatricula(Integer.toString(idM));
        Alumno a = objetoAlumno(matricula);
        Curso c = objetoCurso(matricula);
        if (a != null) {
            matricula.setAlumno(a);
        } 
        if (c != null){
            matricula.setCurso(c);
        }
        idM++;
        bbdd.store(matricula);
        desconectarBd();
        System.out.println("Matrícula " + matricula.getAlumno().getNombre() + " - " + matricula.getCurso().getNombre() + " creada correctamente.");
        f.info("Matrícula " + matricula.getAlumno().getNombre() + " - " + matricula.getCurso().getNombre() + " creada correctamente.");
    }
    
    @Override
    public ArrayList readAlumno() {
        alumnos = new ArrayList();
        alumno = new Alumno(null, null, null, 0, null);
        conectarBd();
        os = bbdd.queryByExample(alumno);
        while (os.hasNext()) {
            Alumno a = (Alumno)os.next();
            alumnos.add(a);  
        }
        desconectarBd();
        return alumnos;
    }
    
    @Override
    public ArrayList readCurso() {
        cursos = new ArrayList();
        curso = new Curso();
        conectarBd();
        os = null;
        os = bbdd.queryByExample(curso);
        while (os.hasNext()) {
            curso = (Curso)os.next();
            cursos.add(curso);
        }
        desconectarBd();
        return cursos;
    }
    
    @Override
    public ArrayList readMatricula() {
        matriculas = new ArrayList();
        matricula = new Matricula();
        conectarBd();
        os = null;
        os = bbdd.queryByExample(matricula);
        while (os.hasNext()) {
            matricula = (Matricula)os.next();
            matriculas.add(matricula);
        }
        desconectarBd();
        return matriculas;
    }

    @Override
    public void update(Alumno alumno) {
        conectarBd();  
        Alumno al = new Alumno(alumno.getId(), null, null, 0, null);
        os = null;
        os = bbdd.queryByExample(al);
        if (os.hasNext()) {
            Alumno a = (Alumno)os.next();
            a.setNombre(alumno.getNombre());
            a.setDni(alumno.getDni());
            a.setEdad(alumno.getEdad());
            a.setEmail(alumno.getEmail());
            bbdd.store(a);
            System.out.println("Alumno " + a.getNombre() + " actualizado correctamente.");
            f.info("Alumno " + a.getNombre() + " actualizado correctamente.");
        }
        desconectarBd();
    }

    @Override
    public void update(Curso curso) {
        conectarBd();
        Curso cur = new Curso(curso.getIdCurso(), null, 0);
        os = null;
        os = bbdd.queryByExample(cur);
        if (os.hasNext()) {
            Curso c = (Curso)os.next();
            c.setNombre(curso.getNombre());
            c.setHoras(curso.getHoras());
            bbdd.store(c);
            System.out.println("Curso " + c.getNombre() + " actualizado correctamente.");
            f.info("Curso " + c.getNombre() + " actualizado correctamente.");
        }
        desconectarBd();
    }

    @Override
    public void update(Matricula matricula) {
        conectarBd();
        Matricula mat = new Matricula(matricula.getIdMatricula(), null, null, null);
        os = null;
        os = bbdd.queryByExample(mat);
        if (os.hasNext()) {
            Matricula m = (Matricula)os.next();
            m.setFecha(matricula.getFecha());
            Alumno a = objetoAlumno(matricula);
            Curso c = objetoCurso(matricula);
            if (a != null) {
                m.setAlumno(a);
            } 
            if (c != null){
                m.setCurso(c);
            }
            bbdd.store(m);
            System.out.println("Matrícula " + matricula.getAlumno().getNombre() + " - " + matricula.getCurso().getNombre() + " actualizada correctamente.");
            f.info("Matrícula " + matricula.getAlumno().getNombre() + " - " + matricula.getCurso().getNombre() + " actualizada correctamente.");
        }
        desconectarBd();
    }

    @Override
    public void delete(Alumno alumno) {
        conectarBd();  
        Alumno al = new Alumno(alumno.getId(), null, null, 0, null);
        os = null;
        os = bbdd.queryByExample(al);
        if (os.hasNext()) {
            Alumno a = (Alumno)os.next();
            bbdd.delete(a);
            System.out.println("Alumno " + a.getNombre() + "borrado correctamente.");
            f.info("Alumno " + a.getNombre() + " borrado correctamente.");
        }
        desconectarBd();
    }

    @Override
    public void delete(Curso curso) {
        conectarBd();
        Curso cur = new Curso(curso.getIdCurso(), null, 0);
        os = null;
        os = bbdd.queryByExample(cur);
        if (os.hasNext()) {
            Curso c = (Curso)os.next();
            bbdd.delete(c);
            System.out.println("Curso " + c.getNombre() + "borrado correctamente.");
            f.info("Curso " + c.getNombre() + " borrado correctamente.");
        }
        desconectarBd();
    }

    @Override
    public void delete(Matricula matricula) {
        conectarBd();
        Matricula mat = new Matricula(matricula.getIdMatricula(), null, null, null);
        os = null;
        os = bbdd.queryByExample(mat);
        if (os.hasNext()) {
            Matricula m = (Matricula)os.next();
            bbdd.delete(m);
            System.out.println("Matrícula " + matricula.getAlumno().getNombre() + " - " + matricula.getCurso().getNombre() + " borrada correctamente.");
            f.info("Matrícula " + matricula.getAlumno().getNombre() + " - " + matricula.getCurso().getNombre() + " borrada correctamente.");
        }
        desconectarBd();
    }
    
    @Override
    public Alumno search(Alumno alumno) {
        conectarBd();  
        Alumno al = new Alumno(alumno.getId(), null, null, 0, null);
        alumno = null;
        os = null;
        os = bbdd.queryByExample(al);
        if (os.hasNext()) {
            Alumno a = (Alumno)os.next();
            alumno = a;
        } 
        if (alumno == null) {
            f.error("El ID proporcionado no corresponde a ningún alumno.");
        }
        desconectarBd();
        return alumno;
    }
    
    @Override
    public Curso search(Curso curso) {
        conectarBd();
        Curso cur = new Curso(curso.getIdCurso(), null, 0);
        curso = null;
        os = null;
        os = bbdd.queryByExample(cur);
        if (os.hasNext()) {
            Curso c = (Curso)os.next();
            curso = c;
        }
        if (curso == null) {
            f.error("El ID proporcionado no corresponde a ningún curso.");
        }
        desconectarBd();
        return curso;
    }
    
    @Override
    public Matricula search(Matricula matricula) {
        conectarBd();
        Matricula mat = new Matricula(matricula.getIdMatricula(), null, null, null);
        matricula = null;
        os = null;
        os = bbdd.queryByExample(mat);
        if (os.hasNext()) {
            Matricula m = (Matricula)os.next();
            matricula = m;
        }
        if (matricula == null) {
            f.error("El ID proporcionado no corresponde a ninguna matricula.");
        }
        desconectarBd();
        return matricula;
    }
    
    /* Otras funciones */
    //Calcular la ID actual
    private int idAlumno() {
        int idActual = 0;
        alumno = new Alumno(null, null, null, 0, null);
        conectarBd();
        os = null;
        os = bbdd.queryByExample(alumno);
        while (os.hasNext()) {
            Alumno a = (Alumno)os.next();
            String id = a.getId();
            if (Integer.parseInt(id) > idActual) {
                idActual = Integer.parseInt(id);
            } 
        }
        desconectarBd();
        return idActual + 1;
    }
    
    private int idCurso() {
        int idActual = 0;
        curso = new Curso(null, null, 0);
        conectarBd();
        os = null;
        os = bbdd.queryByExample(curso);
        while (os.hasNext()) {
            Curso c = (Curso)os.next();
            String id = c.getIdCurso();
            if (Integer.parseInt(id) > idActual) {
                idActual = Integer.parseInt(id);
            } 
        }
        desconectarBd();
        return idActual + 1;
    }
    
    private int idMatricula() {
        int idActual = 0;
        matricula = new Matricula(null, null, null, null);
        conectarBd();
        os = null;
        os = bbdd.queryByExample(matricula);
        while (os.hasNext()) {
            Matricula m = (Matricula)os.next();
            String id = m.getIdMatricula();
            if (Integer.parseInt(id) > idActual) {
                idActual = Integer.parseInt(id);
            } 
        }
        desconectarBd();
        return idActual + 1;
    }
    
    //Obtener el objeto exacto
    private Alumno objetoAlumno(Matricula matricula) {
        alumno = null;
        Alumno a = new Alumno(matricula.getAlumno().getId(), null, null, 0, null);
        os = null;
        os = this.bbdd.queryByExample(a);
        alumno = (Alumno)os.next();
        return alumno;
    }
    
    private Curso objetoCurso(Matricula matricula) {
        curso = null;
        Curso c = new Curso(matricula.getCurso().getIdCurso(), null, 0);
        os = null;
        os = this.bbdd.queryByExample(c);
        curso = (Curso)os.next();
        return curso;
    }

    @Override
    public void crearTablas() {
        //No procede
    }

    @Override
    public void crearDatos() {
        //No procede
    }
}