package org.ceedcv.ceed77prgpro9.modelo;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class Persona {
  
  //Variables de la clase
  protected String idPersona;
  protected String nombre;
  protected String dni;
  
  //Constructores de la clase
  public Persona () {
      
  }
  
  public Persona (String idP, String n, String d) {
    idPersona = idP;
    nombre = n;
    dni = d;
  }
  
  //Métodos de la clase
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
 
  public void setDni(String dni) {
    this.dni = dni;
  }
  
  public void setId(String idPersona) {
    this.idPersona = idPersona;
  }
  
  public String getNombre() {
    return nombre;
  }
  
  public String getDni() {
    return dni;
  }
  
  public String getId() {
    return idPersona;
  }
}
