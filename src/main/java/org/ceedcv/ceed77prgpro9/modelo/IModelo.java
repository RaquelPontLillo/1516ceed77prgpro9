package org.ceedcv.ceed77prgpro9.modelo;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public interface IModelo {
    	//Alumno
	public void create(Alumno alumno);
	public ArrayList<Alumno> readAlumno();
	public void update(Alumno alumno);
	public void delete(Alumno alumno);
        public Alumno search(Alumno alumno);
	
	//Curso
	public void create(Curso curso);
	public ArrayList<Curso> readCurso();
	public void update(Curso curso);
	public void delete(Curso curso);
        public Curso search(Curso curso);
	
	//Matricula
	public void create(Matricula matricula);
	public ArrayList<Matricula> readMatricula();
	public void update(Matricula matricula);
	public void delete(Matricula matricula);
        public Matricula search(Matricula matricula);
        
        //Base de datos
        public void crearBd();
        public void crearTablas();
        public void crearDatos();
        public void conectarBd();
        public void desconectarBd();
        public void eliminarBd();
}