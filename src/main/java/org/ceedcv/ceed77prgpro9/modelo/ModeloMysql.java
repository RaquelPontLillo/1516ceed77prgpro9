package org.ceedcv.ceed77prgpro9.modelo;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.PooledConnection;
import org.ceedcv.ceed77prgpro9.vista.Funciones;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class ModeloMysql implements IModelo {
    public static final SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private final String bbdd = "1516ceed77prg";
    private final String usuario = "alumno";
    private final String password = "alumno";
    private final String jdbcUrl = "jdbc:mysql://localhost/" + bbdd;
    private final String driver = "com.mysql.jdbc.Driver";
    private MysqlConnectionPoolDataSource mcpds = new MysqlConnectionPoolDataSource();
    private Funciones f = new Funciones();
    private Alumno alumno;
    private Curso curso;
    private Matricula matricula;
    private ArrayList alumnos, cursos, matriculas;
    private PooledConnection pc = null;
    private Connection cn = null;
    private Statement st = null;
    private ResultSet rs = null;
    private String error, sql;
    
    
    public ModeloMysql() {
        //Configuración del pool de conexiones
        mcpds.setUser(usuario);
        mcpds.setPassword(password);
        mcpds.setUrl(jdbcUrl);
    }
    
    public void conectarBd() {
        try {
            pc = mcpds.getPooledConnection();
            cn = pc.getConnection();
            System.out.println("Base de datos " + bbdd + ": conectada");
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void desconectarBd() {
        try {
            System.out.println("Base de datos " + bbdd + ": desconectada");
            cn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }
    
    public void crearBd() {
        st = null;
        error = null;  
        try {
            Class.forName(driver).newInstance();
            cn = ((Connection)DriverManager.getConnection("jdbc:mysql://localhost/", usuario, password));
            st = cn.createStatement();  
            sql = "CREATE DATABASE IF NOT EXISTS " + bbdd + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
            f.info("Base de datos " + bbdd + " creada correctamente");
        } catch (Exception e) {
            error = e.getMessage();
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    error = e.getMessage();
                }
            }
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException se) {
                    error = se.getMessage();
                }
            }
        }
        if (error != null) {
            f.error("Ha ocurrido un error durante la instalación \n "
               + "Error: " + error);
        }
    }
    
    @Override
    public void crearTablas() {
        error = null;
        try {
            conectarBd();
            st = cn.createStatement();
            
            sql = "DROP TABLE IF EXISTS alumnos;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "DROP TABLE IF EXISTS cursos;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "DROP TABLE IF EXISTS matriculas;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "CREATE TABLE IF NOT EXISTS `cursos` (\n"
               + "`idc` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "`nombrec` varchar(35) NOT NULL,\n"
               + "`horas` int(5),\n"
               + "PRIMARY KEY (`idc`)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "CREATE TABLE IF NOT EXISTS `alumnos` (\n"
               + "  `ida` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "  `nombrea` varchar(25),\n"                   
               + "  `dni` varchar(11),\n"
               + "  `edad` int(11) ,\n"
               + "  `email` varchar(25),\n"
               + "  PRIMARY KEY (`ida`)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;";
            System.out.println(sql);
            st.executeUpdate(sql);
            
           sql = "CREATE TABLE IF NOT EXISTS `matriculas` (\n"
               + "  `idm` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "  `fecha` date,\n"
               + "  `idc` int(5),\n"
               + "  `ida` int(5),\n"
               + "  PRIMARY KEY (`idm`),\n"    
               + "  FOREIGN KEY (`idc`) REFERENCES cursos(idc),\n"
               + "  FOREIGN KEY (`ida`) REFERENCES alumnos(ida)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;";
            System.out.println(sql);
            st.executeUpdate(sql);

            f.info("Tablas creadas correctamente");
            st.close();
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
            f.error("Ha ocurrido un error durante la creación de las tablas. \n"
                    + "Error:" + error);
        }
        desconectarBd();
    }
    
    @Override
    public void crearDatos() {
        error = null;
        try {
            conectarBd();
            st = cn.createStatement();
            
            sql = "INSERT INTO cursos(idc,nombrec,horas) VALUES(1,'Lengua',20)\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "INSERT INTO cursos(idc,nombrec,horas) VALUES(2,'Matemáticas',40);\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO alumnos(ida,nombrea,dni,edad,email) VALUES(1,'Juan','44522840-S',25,'juan@gmail.com');\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "INSERT INTO alumnos(ida,nombrea,dni,edad,email) VALUES(2,'Luis','44522840-S',39,'luis@gmail.com');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "INSERT INTO matriculas(fecha,idc,ida) VALUES('2016-03-09',1,1);\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "INSERT INTO matriculas(fecha,idc,ida) VALUES('2016-03-21',2,2);\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            f.info("Datos creados correctamente");
            st.close();
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
            f.error("Ha ocurrido un error durante la inserción de datos en la base de datos. \n"
                    + "Error:" + error);
        }
        desconectarBd();
    }
    
    @Override
    public void eliminarBd() {
        cn = null;
  	st = null;
  	error = null;
  	try {
            conectarBd();
            st = cn.createStatement();
            sql = "DROP DATABASE IF EXISTS " + bbdd + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
  	} catch (Exception e) {
            error = e.getMessage();
  	} finally {
            if (st != null) {
                    try {
                    st.close();
                    } catch (SQLException e) {
                        error = e.getMessage();
                    }
            }
            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException se) {
                    error = se.getMessage();
                }
            }
        }
    }
    
    @Override
    public void create(Alumno alumno) {
        try {
            conectarBd();
            st = cn.createStatement();
            String nombre = alumno.getNombre();
            String dni = alumno.getDni();
            int edad = alumno.getEdad();
            String email = alumno.getEmail();
      
            sql = "INSERT INTO alumnos(nombrea,dni,edad,email) VALUES('" + nombre + "','" + dni + "','" + edad + "','" + email + "');";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            f.info("Alumno " + nombre + " creado correctamente");
            desconectarBd();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }    
    
    @Override
    public void create(Curso curso) {
        try {
            conectarBd();
            st = cn.createStatement();
            String nombre = curso.getNombre();
            int horas = curso.getHoras();
      
            sql = "INSERT INTO cursos(nombrec,horas) VALUES('" + nombre + "'," + horas + ");";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            f.info("Curso " + nombre + " creado correctamente");
            desconectarBd();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }
        
    @Override
    public void create(Matricula matricula) {
        try {
            conectarBd();
            st = cn.createStatement();
            Date fecha = matricula.getFecha();
            int idc = Integer.parseInt(matricula.getCurso().getIdCurso());
            int ida = Integer.parseInt(matricula.getAlumno().getId());
      
            sql = "INSERT INTO matriculas(fecha,idc,ida)\n "
                + "VALUES('" + SHORT_DATE_FORMAT.format(fecha) + "','" + idc + "','" + ida + "');";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            f.info("Matrícula creada correctamente");
            desconectarBd();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }
    
    @Override
    public ArrayList readAlumno() {
        alumnos = new ArrayList();
        try {
            conectarBd();
            st = cn.createStatement();
            sql = "SELECT ida,nombrea,dni,edad,email\n"
                + "FROM alumnos\n"
                + "ORDER BY ida;";
            System.out.println(sql);
            rs = st.executeQuery(sql);

            while (rs.next()) {
                alumno = new Alumno();
                
                alumno.setId(rs.getString("ida"));
                alumno.setNombre(rs.getString("nombrea"));
                alumno.setDni(rs.getString("dni"));
                alumno.setEdad(rs.getInt("edad"));
                alumno.setEmail(rs.getString("email"));
                
                alumnos.add(alumno);
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        desconectarBd();
        return alumnos;
    }
    
    @Override
    public ArrayList readCurso() {
        cursos = new ArrayList();
        try {
            conectarBd();
            st = cn.createStatement();
            sql = "SELECT idc,nombrec,horas\n"
                + "FROM cursos\n"
                + "ORDER BY idc;";
            System.out.println(sql);
            rs = st.executeQuery(sql);

            while (rs.next()) {
                curso = new Curso();
        
                curso.setIdCurso(rs.getString("idc"));
                curso.setNombre(rs.getString("nombrec"));
                curso.setHoras(rs.getInt("horas"));
                
                cursos.add(curso);
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        desconectarBd();
        return cursos;
    }
    
    @Override
    public ArrayList readMatricula() {
        matriculas = new ArrayList();
        try {
            conectarBd();
            st = cn.createStatement();
            sql = "SELECT m.idm,m.fecha,c.idc,c.nombrec,c.horas,a.ida,a.nombrea,a.dni,a.edad,a.email \n"
                + "FROM matriculas as m,cursos as c, alumnos as a \n"
                + "WHERE m.idc = c.idc AND m.ida = a.ida \n "
                + "ORDER BY idm;";
            System.out.println(sql);
            rs = st.executeQuery(sql);

            while (rs.next()) {
                matricula = new Matricula();
                alumno = new Alumno();
                curso = new Curso();
                
                alumno.setId(rs.getString("ida"));
                alumno.setNombre(rs.getString("nombrea"));
                alumno.setDni(rs.getString("dni"));
                alumno.setEdad(rs.getInt("edad"));
                alumno.setEmail(rs.getString("email"));

                curso.setIdCurso(rs.getString("idc"));
                curso.setNombre(rs.getString("nombrec"));
                curso.setHoras(rs.getInt("horas"));
                
                matricula.setIdMatricula(rs.getString("idm"));
                matricula.setFecha(SHORT_DATE_FORMAT.parse(rs.getString("fecha")));
                matricula.setCurso(curso);
                matricula.setAlumno(alumno);
                matriculas.add(matricula);
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        desconectarBd();
        return matriculas;
    }

    @Override
    public void update(Alumno alumno) {
        try {
            conectarBd();
            st = cn.createStatement();
            sql = "UPDATE alumnos  "
                + "SET nombrea='" + alumno.getNombre()
                +  "', dni='" + alumno.getDni()
                +  "', edad=" + alumno.getEdad()
                +  ", email='" + alumno.getEmail()
                +  "' WHERE ida=" + alumno.getId() + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            f.info("Alumno " + alumno.getNombre() + " actualizado correctamente");
            desconectarBd();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    @Override
    public void update(Curso curso) {
        try {
            conectarBd();
            st = cn.createStatement();
            sql = "UPDATE cursos  "
                + "SET nombrec='" + curso.getNombre()
                +  "', horas=" + curso.getHoras()
                +  " WHERE idc=" + curso.getIdCurso() + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            f.info("Curso " + curso.getNombre() + " actualizado correctamente");
            desconectarBd();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    @Override
    public void update(Matricula matricula) {
        try {
            conectarBd();
            st = cn.createStatement();
            sql = "UPDATE matriculas  "
                + "SET fecha='" + SHORT_DATE_FORMAT.format(matricula.getFecha())
                +  "', idc=" + matricula.getCurso().getIdCurso()
                +  ", ida=" + matricula.getAlumno().getId()
                +  " WHERE idm=" + matricula.getIdMatricula() + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            f.info("Matrícula actualizada correctamente");
            desconectarBd();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    @Override
    public void delete(Alumno alumno) {
        try {
            conectarBd();
            st = cn.createStatement();
            sql = "DELETE FROM alumnos  "
                +  " WHERE ida=" + alumno.getId() + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            f.info("Alumno " + alumno.getNombre() + " borrado correctamente");
            desconectarBd();
        } catch (SQLException se) {
            se.printStackTrace();
            f.error("El alumno no ha podido borrarse. Consultar si tiene matrículas asociadas, y eliminar primero la matrícula. \n"
                    + "Error: " + se.getMessage());
        }
    }

    @Override
    public void delete(Curso curso) {
        try {
            conectarBd();
            st = cn.createStatement();
            sql = "DELETE FROM cursos  "
                +  " WHERE idc=" + curso.getIdCurso() + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
            desconectarBd();
        } catch (SQLException se) {
            se.printStackTrace();
            f.error("El curso no ha podido borrarse. Consultar si tiene matrículas asociadas, y eliminar primero la matrícula. \n"
                    + "Error: " + se.getMessage());
        }
    }

    @Override
    public void delete(Matricula matricula) {
        try {
            conectarBd();
            st = cn.createStatement();
            sql = "DELETE FROM matriculas  "
                +  " WHERE idm=" + matricula.getIdMatricula() + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
            desconectarBd();
        } catch (SQLException se) {
            se.printStackTrace();
            f.error("La matrícula no ha podido borrarse. \n"
                    + "Error: " + se.getMessage());
        }        
    }
    
    @Override
    public Alumno search(Alumno alumno) {
        Alumno a = null;
        try {
            conectarBd();
            st = cn.createStatement();
            sql = "SELECT ida,nombrea,dni,edad,email\n"
                + "FROM alumnos\n"
                + "WHERE ida=" + alumno.getId() +";";
            System.out.println(sql);
            rs = st.executeQuery(sql);
        
            while (rs.next()) {
                a = new Alumno();

                a.setId(rs.getString("ida"));
                a.setNombre(rs.getString("nombrea"));
                a.setDni(rs.getString("dni"));
                a.setEdad(rs.getInt("edad"));
                a.setEmail(rs.getString("email"));
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        desconectarBd();
        if (a == null) {
            f.error("El ID proporcionado no corresponde a ningún alumno");
        }
        return a;
    }
    
    @Override
    public Curso search(Curso curso) {
        Curso c = null;
        try {
            conectarBd();
            st = cn.createStatement();
            sql = "SELECT idc, nombrec, horas\n"
                + " FROM cursos\n"
                + " WHERE idc=" + curso.getIdCurso() + ";";
            System.out.println(sql);
            rs = st.executeQuery(sql);
        
            while (rs.next()) {
                c = new Curso();

                c.setIdCurso(rs.getString("idc"));
                c.setNombre(rs.getString("nombrec"));
                c.setHoras(rs.getInt("horas"));
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        desconectarBd();
        if (c == null) {
            f.error("El ID proporcionado no corresponde a ningún curso");
        }
        return c;
    }
    
    @Override
    public Matricula search(Matricula matricula) {
        Matricula m = null;
        try {
            conectarBd();
            st = cn.createStatement();
            sql = "SELECT m.idm,m.fecha,c.idc,c.nombrec,c.horas,a.ida,a.nombrea,a.dni,a.edad,a.email \n"
                + "FROM matriculas as m,cursos as c, alumnos as a \n"
                + "WHERE m.idc = c.idc AND m.ida = a.ida "
                + "AND idm=" + matricula.getIdMatricula() + ";";
            System.out.println(sql);
            rs = st.executeQuery(sql);
            
            while (rs.next()) {
                m = new Matricula();
                alumno = new Alumno();
                curso = new Curso();
                
                alumno.setId(rs.getString("ida"));
                alumno.setNombre(rs.getString("nombrea"));
                alumno.setDni(rs.getString("dni"));
                alumno.setEdad(rs.getInt("edad"));
                alumno.setEmail(rs.getString("email"));
 
                curso.setIdCurso(rs.getString("idc"));
                curso.setNombre(rs.getString("nombrec"));
                curso.setHoras(rs.getInt("horas"));
                
                m.setIdMatricula(rs.getString("idm"));
                m.setFecha(SHORT_DATE_FORMAT.parse(rs.getString("fecha")));
                m.setCurso(curso);
                m.setAlumno(alumno);
            }   
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        desconectarBd();
        if (m == null) {
            f.error("El ID proporcionado no corresponde a ninguna matrícula");
        }
        return m;
    }
}