package org.ceedcv.ceed77prgpro9.modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ceedcv.ceed77prgpro9.vista.Funciones;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class ModeloFichero implements IModelo {
    public static final SimpleDateFormat SHORT_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
    private Funciones f = new Funciones();
    File alumnos = new File("FicheroAlumnos.csv"); //creamos el archivo para guardar los alumnos
    File cursos = new File("FicheroCursos.csv"); //creamos el archivo para guardar los cursos
    File matriculas = new File("FicheroMatriculas.csv"); //creamos el archivo para guardar las matrículas
    
    public ModeloFichero() throws IOException {

    }
    
    @Override
    public void create(Alumno alumno) {
        ArrayList array = readAlumno();
        Iterator iterator = array.iterator();
        
        alumno.setId(Integer.toString(idAlumno()));
        
        //Comprobamos la existencia de la ID
        while (iterator.hasNext()) {
            Alumno a = (Alumno) iterator.next();
            if (alumno.getId().equals(a.getId())) {
                f.error("Ya existe un alumno con la ID proporcionada.");
                return;
            }
        }

        try {
            FileWriter filewriter = new FileWriter(alumnos, true); //creamos un objeto filewriter para utilizarlo sobre el fichero de alumnos
            filewriter.write(alumno.getId() + ";" + alumno.getNombre() + ";" + alumno.getDni() + ";" + alumno.getEdad() 
                + ";" + alumno.getEmail() + ";\r\n"); //escribimos el objeto en el fichero separando los datos con ;
            filewriter.close(); //cerramos el fichero de alumnos
            f.info("Alumno dado de alta con éxito."); //mostramos mensaje informativo
        } catch (IOException e) { //capturamos la excepción 
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, e); //mostramos el error
        }
    }
    
    @Override
    public void create(Curso curso) {
        ArrayList array = readCurso();
        Iterator iterator = array.iterator();
        curso.setIdCurso(Integer.toString(idCurso()));
        
        //Comprobamos la existencia de la ID
        while (iterator.hasNext()) {
            Curso c = (Curso) iterator.next();
            if (curso.getIdCurso().equals(c.getIdCurso())) {
                f.error("Ya existe un curso con la ID proporcionada.");
                return;
            }
        }
        
        try {
            FileWriter filewriter = new FileWriter(cursos, true); //creamos un objeto filewriter para utilizarlo sobre el fichero de cursos
            filewriter.write(curso.getIdCurso() + ";" + curso.getNombre() + ";" 
                + curso.getHoras() + ";\r\n"); //escribimos el objeto en el fichero separando los datos con ;
            filewriter.close(); //cerramos el fichero de cursos
            f.info("Curso dado de alta con éxito."); //mostramos mensaje informativo
        } catch (IOException ex) { //capturamos la excepción 
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex); //mostramos el error
        }
    }
    
    @Override
    public void create(Matricula matricula) {
        matricula.setIdMatricula(Integer.toString(idMatricula()));
        ArrayList array = readMatricula();    //Creamos un arrayList de matrículas
        Iterator iterator = array.iterator();      //Creamos un objeto Iterator para trabajar con el arrayList
        while (iterator.hasNext()) {                    //Iteramos el arrayList completo
            Matricula m = (Matricula) iterator.next();
            if (matricula.getIdMatricula().equals(m.getIdMatricula())) { //Si el ID que pone el usuario ya existe TERMINAMOS LA FUNCIÓN con un error
                   f.error("Ya existe una matrícula con la ID proporcionada.");  //Arrojamos error
                return;         //Terminamos la función, no permitimos crear una matrícula con un ID existente
            }
        }
   
        try {
            FileWriter filewriter = new FileWriter(matriculas, true); //creamos un objeto filewriter para utilizarlo sobre el fichero matriculas
            filewriter.write(matricula.getIdMatricula() + ";" + SHORT_DATE_FORMAT.format(matricula.getFecha()) + ";" + matricula.getAlumno().getId()+ ";" + matricula.getCurso().getIdCurso() + ";\r\n"); 
            //escribimos el objeto en el fichero separando los datos con ;
            filewriter.close(); //cerramos el fichero de alumnos
            f.info("Matrícula dada de alta con éxito."); //mostramos mensaje informativo
        } catch (IOException ex) { //capturamos la excepción 
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex); //mostramos el error
        } 
    }
    
    @Override
    public ArrayList readAlumno() {
        ArrayList hashset = new ArrayList();
        try {
            FileReader filereader = new FileReader(alumnos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idAlumno = stokenizer.nextToken();
                String nAlumno = stokenizer.nextToken();
                String dni = stokenizer.nextToken();
                int edad = Integer.parseInt(stokenizer.nextToken());
                String email = stokenizer.nextToken();
                Alumno alumno = new Alumno(idAlumno, nAlumno, dni, edad, email);
                hashset.add(alumno);
                string = bufferedreader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hashset;
    }
    
    @Override
    public ArrayList<Curso> readCurso() {
        ArrayList array = new ArrayList();
        try {
            FileReader filereader = new FileReader(cursos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
            StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idCurso = stokenizer.nextToken();
                String nombre = stokenizer.nextToken();
                int horas = Integer.parseInt(stokenizer.nextToken());
                Curso curso = new Curso(idCurso, nombre, horas);
                array.add(curso);
                string = bufferedreader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }
    
    @Override
    public ArrayList<Matricula> readMatricula() {
        ArrayList array = new ArrayList();
        try {
            FileReader filereader = new FileReader(matriculas);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                //Recogemos las variables del objeto matricula
                String idMatricula = stokenizer.nextToken();
                Date fecha = null;
                try {
                    fecha = SHORT_DATE_FORMAT.parse(stokenizer.nextToken());                
                } catch (ParseException pe) {
                    f.error("No se ha podido convertir el campo String a Date.");
                }
                //Recogemos las variables del objeto alumno
                String idAlumno = stokenizer.nextToken();
                //Recogemos las variables del objeto curso
                String idCurso = stokenizer.nextToken();
                Alumno a = new Alumno(idAlumno, "", "", 0, "");
                Curso c = new Curso(idCurso, "", 0);
                Curso curso = search(c);
                Alumno alumno = search(a);
                Matricula matricula = new Matricula(idMatricula, fecha, curso, alumno);
                array.add(matricula);
                string = bufferedreader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }

    @Override
    public void update(Alumno alumno) {
        File temp = new File("temp.csv");
        Funciones f = new Funciones();
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(alumnos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idAlumno = stokenizer.nextToken();
                String nAlumno = stokenizer.nextToken();
                String dni = stokenizer.nextToken();
                int edad = Integer.parseInt(stokenizer.nextToken());
                String email = stokenizer.nextToken();
                Alumno a = new Alumno(idAlumno, nAlumno, dni, edad, email);
                if (alumno.getId().equals(a.getId())) {
                    filewriter.write(alumno.getId() + ";" + alumno.getNombre() + ";" + alumno.getDni() + ";" + alumno.getEdad() + ";" 
                        + alumno.getEmail() + ";\r\n"); 
                } else {
                    filewriter.write(a.getId() + ";" + a.getNombre() + ";" + a.getDni() + ";" + a.getEdad() + ";" 
                        + a.getEmail() + ";\r\n");
                }
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
            f.info("Alumno actualizado con éxito.");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        alumnos.delete();
        temp.renameTo(new File("FicheroAlumnos.csv"));
    }
    
    @Override
    public void update(Curso curso) {
        File temp = new File("temp.csv");
        Funciones f = new Funciones();
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(cursos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idCurso = stokenizer.nextToken();
                String nCurso = stokenizer.nextToken();
                int horas = Integer.parseInt(stokenizer.nextToken());
                Curso c = new Curso(idCurso, nCurso, horas);
                if (curso.getIdCurso().equals(c.getIdCurso())) {
                    filewriter.write(curso.getIdCurso() + ";" + curso.getNombre() + ";" + curso.getHoras() + ";\r\n"); 
                } else {
                    filewriter.write(c.getIdCurso() + ";" + c.getNombre() + ";" + c.getHoras() + ";\r\n");
                }
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
            f.info("Curso actualizado con éxito.");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        cursos.delete();
        temp.renameTo(new File("FicheroCursos.csv"));
    }
    
    @Override
    public void update(Matricula matricula) {
        File temp = new File("temp.csv");
        Funciones f = new Funciones();
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(matriculas);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                //Recogemos las variables del objeto matricula
                String idMatricula = stokenizer.nextToken();
                Date fecha = null;
                try {
                    fecha = SHORT_DATE_FORMAT.parse(stokenizer.nextToken());                
                } catch (ParseException pe) {
                    f.error("No se ha podido convertir el campo Date a String.");
                }
                //Recogemos las variables del objeto alumno
                String idAlumno = stokenizer.nextToken();
                //Recogemos las variables del objeto curso
                String idCurso = stokenizer.nextToken();
                Alumno alumno = new Alumno(idAlumno, "", "", 0, "");
                Curso curso = new Curso(idCurso, "", 0);
                Matricula m = new Matricula(idMatricula, fecha, curso, alumno);
                if (matricula.getIdMatricula().equals(m.getIdMatricula())) {
                    filewriter.write(matricula.getIdMatricula() + ";" + SHORT_DATE_FORMAT.format(matricula.getFecha()) + ";" + matricula.getAlumno().getId()+ ";" + matricula.getCurso().getIdCurso() + ";\r\n");
                } else {
                    filewriter.write(m.getIdMatricula() + ";" + SHORT_DATE_FORMAT.format(m.getFecha()) + ";" + m.getAlumno().getId() + ";" + m.getCurso().getIdCurso() + ";\r\n");
                }
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
            f.info("Matrícula actualizada con éxito.");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        matriculas.delete();
        temp.renameTo(new File("FicheroMatriculas.csv"));
    }

    @Override
    public void delete(Alumno alumno) {
        Funciones f = new Funciones();
        File temp = new File("temp.csv");
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(alumnos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idAlumno = stokenizer.nextToken();
                String nAlumno = stokenizer.nextToken();
                String dni = stokenizer.nextToken();
                int edad = Integer.parseInt(stokenizer.nextToken());
                String email = stokenizer.nextToken();
                Alumno a = new Alumno(idAlumno, nAlumno, dni, edad, email);
                if (!alumno.getId().equals(a.getId())) {
                    filewriter.write(a.getId() + ";" + a.getNombre() + ";" + a.getDni() + ";"+ a.getEdad() + ";" + a.getEmail() + ";\r\n");
                } 
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
            f.info("Alumno borrado con éxito.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        alumnos.delete();
        temp.renameTo(new File("FicheroAlumnos.csv"));
    }

    @Override
    public void delete(Curso curso) {
        Funciones f = new Funciones();
        File temp = new File("temp.csv");
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(cursos);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                String idCurso = stokenizer.nextToken();
                String nombre = stokenizer.nextToken();
                int horas = Integer.parseInt(stokenizer.nextToken());
                Curso c = new Curso(idCurso, nombre, horas);
                if (!curso.getIdCurso().equals(c.getIdCurso())) {
                    filewriter.write(c.getIdCurso() + ";" + c.getNombre()+ ";" + c.getHoras() + ";\r\n");
                } 
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
            f.info("Curso borrado con éxito.");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        cursos.delete();
        temp.renameTo(new File("FicheroCursos.csv"));
    }
    
    @Override
    public void delete(Matricula matricula) {
        Funciones f = new Funciones();
        File temp = new File("temp.csv");
        try {
            FileWriter filewriter = new FileWriter(temp, true);
            FileReader filereader = new FileReader(matriculas);
            BufferedReader bufferedreader = new BufferedReader(filereader);
            String string;
            string = bufferedreader.readLine();
            while (string != null) {
                StringTokenizer stokenizer = new StringTokenizer(string, ";");
                //Recogemos las variables del objeto matricula
                String idMatricula = stokenizer.nextToken();
                Date fecha = null;
                try {
                    fecha = SHORT_DATE_FORMAT.parse(stokenizer.nextToken());                
                } catch (ParseException pe) {
                    f.error("No se ha podido convertir el campo Date a String.");
                }
                //Recogemos las variables del objeto alumno
                String idAlumno = stokenizer.nextToken();
                //Recogemos las variables del objeto curso
                String idCurso = stokenizer.nextToken();
                Alumno alumno = new Alumno(idAlumno, "", "", 0, "");
                Curso curso = new Curso(idCurso, "", 0);
                Matricula m = new Matricula(idMatricula, fecha, curso, alumno);
                if (!matricula.getIdMatricula().equals(m.getIdMatricula())) {
                    filewriter.write(m.getIdMatricula() + ";" + SHORT_DATE_FORMAT.format(m.getFecha()) + ";" + m.getAlumno().getId()+ ";" + m.getCurso().getIdCurso() + ";\r\n");
                } 
                string = bufferedreader.readLine();
            }
            filewriter.close();
            filereader.close();
            f.info("Matricula borrada con éxito.");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        matriculas.delete();
        temp.renameTo(new File("FicheroMatriculas.csv"));
    }
    
    @Override
    public Alumno search(Alumno alumno) {
        ArrayList array = readAlumno();
        Iterator iterator = array.iterator();
        
        while (iterator.hasNext()) {
            Alumno a = (Alumno) iterator.next();
            if (alumno.getId().equals(a.getId())) {
                return a;
            }
        }
        return null;
    }
    
    @Override
    public Curso search(Curso curso) {
        ArrayList array = readCurso();
        Iterator iterator = array.iterator();
        
        while (iterator.hasNext()) {
            Curso c = (Curso) iterator.next();
            if (curso.getIdCurso().equals(c.getIdCurso())) {
                return c;
            }
        }
        return null;
    }
    
    @Override
    public Matricula search(Matricula matricula) {
        ArrayList array = readMatricula();
        Iterator iterator = array.iterator();
        
        while (iterator.hasNext()) {
            Matricula m = (Matricula) iterator.next();
            if (matricula.getIdMatricula().equals(m.getIdMatricula())) {
                return m;
            }
        }
        return null;
    }

    //Funciones de la base de datos
    @Override
    public void crearBd() {
        FileWriter filewriter;
        try {
            if (!alumnos.exists()) {
                filewriter = new FileWriter(alumnos);
                filewriter.close();
            }
            if (!cursos.exists()) {
                filewriter = new FileWriter(cursos);
                filewriter.close();
            }
            if (!matriculas.exists()) {
                filewriter = new FileWriter(matriculas);
                filewriter.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            f.error("Ha ocurrido un error durante la creación de los ficheros CSV. \n"
                    + "Error: " + e.getMessage());
        }
    }

    @Override
    public void crearTablas() {
        //No procede
    }

    @Override
    public void crearDatos() {
        //No procede
    }

    @Override
    public void conectarBd() {
        //No procede
    }

    @Override
    public void desconectarBd() {
        //No procede
    }

    @Override
    public void eliminarBd() {
       alumnos.delete();
       cursos.delete();
       matriculas.delete();
       f.info("Ficheros CSV eliminados correctamente.");
    }
    
    //Otras funciones
    private int idAlumno() {
        int idmax = 0;
        if (alumnos.exists()) {
            try {
                FileReader fr = new FileReader(alumnos);
                BufferedReader br = new BufferedReader(fr);

                String linea = br.readLine();
                while (linea != null) {
                    Alumno alumno = obtenerAlumno(linea);
                    int id = Integer.parseInt(alumno.getId());
                    if (id > idmax) {
                        idmax = id;
                    }
                    linea = br.readLine();
                }
            fr.close();
            } catch (FileNotFoundException localFileNotFoundException) {

            } catch (IOException localIOException) {

            }
        }
        return idmax +1;
    }
    
    private int idCurso() {
        int idmax = 0;
        if (cursos.exists()) {
            try {
                FileReader fr = new FileReader(cursos);
                BufferedReader br = new BufferedReader(fr);

                String linea = br.readLine();
                while (linea != null) {
                    Curso curso = obtenerCurso(linea);
                    int id = Integer.parseInt(curso.getIdCurso());
                    if (id > idmax) {
                        idmax = id;
                    }
                    linea = br.readLine();
                }
            fr.close();
            } catch (FileNotFoundException localFileNotFoundException) {

            } catch (IOException localIOException) {

            }
        }
        return idmax +1;
    }

    private int idMatricula() {
        int idmax = 0;
        if (matriculas.exists()) {
            try {
                FileReader fr = new FileReader(matriculas);
                BufferedReader br = new BufferedReader(fr);

                String linea = br.readLine();
                while (linea != null) {
                    Matricula m = obtenerMatricula(linea);
                    int id = Integer.parseInt(m.getIdMatricula());
                    if (id > idmax) {
                        idmax = id;
                    }
                    linea = br.readLine();
                }
            fr.close();
            } catch (FileNotFoundException localFileNotFoundException) {

            } catch (IOException localIOException) {

            }
        }
        return idmax +1;
    }
    
    private Alumno obtenerAlumno(String s) {
        StringTokenizer stokenizer = new StringTokenizer(s, ";");
        String idAlumno = stokenizer.nextToken();
        String nAlumno = stokenizer.nextToken();
        String dni = stokenizer.nextToken();
        int edad = Integer.parseInt(stokenizer.nextToken());
        String email = stokenizer.nextToken();
        Alumno a = new Alumno(idAlumno, nAlumno, dni, edad, email);
        return a;
    }
    
    private Curso obtenerCurso(String s) {
        StringTokenizer stokenizer = new StringTokenizer(s, ";");
        String idCurso = stokenizer.nextToken();
        String nombre = stokenizer.nextToken();
        int horas = Integer.parseInt(stokenizer.nextToken());
        Curso c = new Curso(idCurso, nombre, horas);
        return c;
    }
    
    private Matricula obtenerMatricula(String s) {
        StringTokenizer stokenizer = new StringTokenizer(s, ";");
        //Recogemos las variables del objeto matricula
        String idMatricula = stokenizer.nextToken();
        Date fecha = null;
        try {
            fecha = SHORT_DATE_FORMAT.parse(stokenizer.nextToken());                
        } catch (ParseException pe) {
            f.error("No se ha podido convertir el campo Date a String.");
        }
        //Recogemos las variables del objeto alumno
        String idAlumno = stokenizer.nextToken();
        //Recogemos las variables del objeto curso
        String idCurso = stokenizer.nextToken();
        Alumno alumno = new Alumno(idAlumno, "", "", 0, "");
        Curso curso = new Curso(idCurso, "", 0);
        Matricula m = new Matricula(idMatricula, fecha, curso, alumno);
        return m;
    }
}