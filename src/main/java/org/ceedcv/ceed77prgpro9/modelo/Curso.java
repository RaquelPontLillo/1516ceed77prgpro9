package org.ceedcv.ceed77prgpro9.modelo;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Curso {
  //Variables de la clase
  private String idCurso;
  private String nombre;
  private int horas;
  
  //Constructores de la clase
  public Curso () {

  }
  
 public Curso (String idC, String n, int h) {
   idCurso = idC;
   nombre = n;
   horas = h;
 }
 
 //Métodos de la clase
 
 //Getters
 public String getIdCurso() {
 return idCurso;
 }
 
 public String getNombre() {
 return nombre;
 }
 
 public int getHoras() {
 return horas;
 }
 
 //Setters
 public void setIdCurso (String idC){
 idCurso = idC;
 }
 
 public void setNombre (String n){
 nombre = n;
 }
 
 public void setHoras (int h){
 horas = h;
 }
}
