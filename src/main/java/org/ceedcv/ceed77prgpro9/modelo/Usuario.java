package org.ceedcv.ceed77prgpro9.modelo;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Usuario extends Persona {
  
  //Variables de la clase
  private final String usuario = "root";
  private final String password = "pass";
  
  //Metodos de la clase
  public String getUsuario () {
    return usuario;
  }
  
  public String getPassword () {
    return password;
  }
}
